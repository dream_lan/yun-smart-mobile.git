# Summary

Date : 2022-04-01 15:59:57

Directory /Users/mac/Desktop/开发项目/Vue/EasySmartMobile

Total : 133 files,  38943 codes, 1724 comments, 952 blanks, all 41619 lines

summary / [details](details.md) / [diff summary](diff.md) / [diff details](diff-details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| Vue | 85 | 21,308 | 1,253 | 663 | 23,224 |
| JSON | 3 | 14,229 | 0 | 2 | 14,231 |
| Lua | 1 | 1,789 | 260 | 74 | 2,123 |
| JavaScript | 15 | 902 | 69 | 78 | 1,049 |
| C++ | 1 | 381 | 61 | 87 | 529 |
| SCSS | 3 | 94 | 15 | 15 | 124 |
| Python | 1 | 84 | 13 | 21 | 118 |
| XML | 21 | 71 | 0 | 2 | 73 |
| HTML | 1 | 58 | 1 | 1 | 60 |
| Markdown | 1 | 19 | 0 | 6 | 25 |
| CSS | 1 | 8 | 52 | 3 | 63 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 133 | 38,943 | 1,724 | 952 | 41,619 |
| public | 2 | 59 | 1 | 1 | 61 |
| python | 1 | 84 | 13 | 21 | 118 |
| src | 124 | 24,503 | 1,696 | 917 | 27,116 |
| src/api | 5 | 225 | 36 | 20 | 281 |
| src/assets | 28 | 351 | 67 | 54 | 472 |
| src/assets/bg | 2 | 26 | 0 | 0 | 26 |
| src/assets/css | 4 | 102 | 67 | 18 | 187 |
| src/assets/img | 5 | 18 | 0 | 1 | 19 |
| src/assets/img/interfacePage_Comment | 1 | 14 | 0 | 1 | 15 |
| src/assets/js | 3 | 178 | 0 | 34 | 212 |
| src/assets/svg | 14 | 27 | 0 | 1 | 28 |
| src/assets/svg/Farm_2 | 5 | 5 | 0 | 0 | 5 |
| src/components | 64 | 18,047 | 1,061 | 615 | 19,723 |
| src/components/Cat_Home | 2 | 683 | 38 | 29 | 750 |
| src/components/Dorm | 2 | 575 | 16 | 13 | 604 |
| src/components/Express1 | 4 | 949 | 59 | 34 | 1,042 |
| src/components/FailDetect_1 | 4 | 990 | 54 | 33 | 1,077 |
| src/components/Farm | 2 | 521 | 24 | 14 | 559 |
| src/components/Farm_2 | 7 | 3,238 | 302 | 102 | 3,642 |
| src/components/InterFacePage | 5 | 1,610 | 153 | 57 | 1,820 |
| src/components/InterFacePage/Info | 2 | 504 | 34 | 25 | 563 |
| src/components/Lab_1 | 2 | 423 | 17 | 12 | 452 |
| src/components/MircoStep_1 | 2 | 508 | 23 | 10 | 541 |
| src/components/Position | 6 | 1,559 | 66 | 54 | 1,679 |
| src/components/Position2 | 6 | 1,542 | 81 | 55 | 1,678 |
| src/components/RemoteAir | 1 | 171 | 2 | 2 | 175 |
| src/components/RemotePower | 1 | 174 | 2 | 2 | 178 |
| src/components/SmartKit | 3 | 920 | 82 | 100 | 1,102 |
| src/components/ToolBar | 4 | 1,219 | 65 | 42 | 1,326 |
| src/components/cDevice1 | 2 | 808 | 48 | 27 | 883 |
| src/components/currency | 1 | 168 | 6 | 3 | 177 |
| src/router | 1 | 281 | 7 | 8 | 296 |
| src/store | 1 | 27 | 1 | 2 | 30 |
| src/utils | 1 | 85 | 3 | 3 | 91 |
| src/views | 21 | 5,159 | 451 | 181 | 5,791 |
| src/views/Home | 2 | 510 | 52 | 12 | 574 |
| src/views/Mine | 5 | 735 | 52 | 34 | 821 |

summary / [details](details.md) / [diff summary](diff.md) / [diff details](diff-details.md)