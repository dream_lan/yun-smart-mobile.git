# Details

Date : 2021-09-17 20:23:28

Directory /Users/mac/Desktop/开发项目/Vue/wsn

Total : 84 files,  27526 codes, 885 comments, 584 blanks, all 28995 lines

[summary](results.md)

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [.eslintrc.js](/.eslintrc.js) | JavaScript | 20 | 0 | 1 | 21 |
| [README.md](/README.md) | Markdown | 19 | 0 | 6 | 25 |
| [babel.config.js](/babel.config.js) | JavaScript | 5 | 0 | 1 | 6 |
| [package-lock.json](/package-lock.json) | JSON | 13,658 | 0 | 1 | 13,659 |
| [package.json](/package.json) | JSON | 58 | 0 | 1 | 59 |
| [public/index.html](/public/index.html) | HTML | 19 | 1 | 1 | 21 |
| [python/fish.py](/python/fish.py) | Python | 84 | 13 | 21 | 118 |
| [src/App.vue](/src/App.vue) | Vue | 197 | 43 | 22 | 262 |
| [src/api/mqtt.js](/src/api/mqtt.js) | JavaScript | 31 | 0 | 1 | 32 |
| [src/api/mqttcontrol.js](/src/api/mqttcontrol.js) | JavaScript | 9 | 0 | 1 | 10 |
| [src/assets/bg/bottom.svg](/src/assets/bg/bottom.svg) | XML | 13 | 0 | 0 | 13 |
| [src/assets/bg/top.svg](/src/assets/bg/top.svg) | XML | 13 | 0 | 0 | 13 |
| [src/assets/css/colors.scss](/src/assets/css/colors.scss) | SCSS | 5 | 4 | 1 | 10 |
| [src/assets/css/dimens.scss](/src/assets/css/dimens.scss) | SCSS | 12 | 3 | 2 | 17 |
| [src/assets/css/reset.css](/src/assets/css/reset.css) | CSS | 8 | 52 | 3 | 63 |
| [src/assets/css/style.scss](/src/assets/css/style.scss) | SCSS | 77 | 8 | 12 | 97 |
| [src/assets/img/beat.svg](/src/assets/img/beat.svg) | XML | 1 | 0 | 0 | 1 |
| [src/assets/img/enclosure.svg](/src/assets/img/enclosure.svg) | XML | 1 | 0 | 0 | 1 |
| [src/assets/img/position.svg](/src/assets/img/position.svg) | XML | 1 | 0 | 0 | 1 |
| [src/assets/img/track.svg](/src/assets/img/track.svg) | XML | 1 | 0 | 0 | 1 |
| [src/assets/js/gloab.js](/src/assets/js/gloab.js) | JavaScript | 6 | 0 | 0 | 6 |
| [src/assets/js/md5.min.js](/src/assets/js/md5.min.js) | JavaScript | 83 | 0 | 19 | 102 |
| [src/assets/svg/beat.svg](/src/assets/svg/beat.svg) | XML | 1 | 0 | 0 | 1 |
| [src/assets/svg/device.svg](/src/assets/svg/device.svg) | XML | 1 | 0 | 0 | 1 |
| [src/assets/svg/humi.svg](/src/assets/svg/humi.svg) | XML | 1 | 0 | 0 | 1 |
| [src/assets/svg/id.svg](/src/assets/svg/id.svg) | XML | 1 | 0 | 0 | 1 |
| [src/assets/svg/mode.svg](/src/assets/svg/mode.svg) | XML | 1 | 0 | 0 | 1 |
| [src/assets/svg/set.svg](/src/assets/svg/set.svg) | XML | 1 | 0 | 0 | 1 |
| [src/assets/svg/temp.svg](/src/assets/svg/temp.svg) | XML | 1 | 0 | 0 | 1 |
| [src/components/Cat_Home/Banner.vue](/src/components/Cat_Home/Banner.vue) | Vue | 150 | 4 | 2 | 156 |
| [src/components/Cat_Home/Index.vue](/src/components/Cat_Home/Index.vue) | Vue | 533 | 34 | 27 | 594 |
| [src/components/Dorm/Banner.vue](/src/components/Dorm/Banner.vue) | Vue | 150 | 4 | 2 | 156 |
| [src/components/Dorm/Index.vue](/src/components/Dorm/Index.vue) | Vue | 425 | 12 | 11 | 448 |
| [src/components/Farm/Banner.vue](/src/components/Farm/Banner.vue) | Vue | 170 | 2 | 3 | 175 |
| [src/components/Farm/Index.vue](/src/components/Farm/Index.vue) | Vue | 351 | 22 | 11 | 384 |
| [src/components/Fish.vue](/src/components/Fish.vue) | Vue | 248 | 2 | 2 | 252 |
| [src/components/InterFacePage/Banner.vue](/src/components/InterFacePage/Banner.vue) | Vue | 329 | 76 | 8 | 413 |
| [src/components/InterFacePage/Index.vue](/src/components/InterFacePage/Index.vue) | Vue | 479 | 17 | 11 | 507 |
| [src/components/InterFacePage/Settings.vue](/src/components/InterFacePage/Settings.vue) | Vue | 219 | 22 | 12 | 253 |
| [src/components/LRBL.vue](/src/components/LRBL.vue) | Vue | 142 | 3 | 3 | 148 |
| [src/components/Lab_1/Banner.vue](/src/components/Lab_1/Banner.vue) | Vue | 149 | 4 | 2 | 155 |
| [src/components/Lab_1/Index.vue](/src/components/Lab_1/Index.vue) | Vue | 274 | 13 | 10 | 297 |
| [src/components/Light.vue](/src/components/Light.vue) | Vue | 239 | 2 | 2 | 243 |
| [src/components/MQ.vue](/src/components/MQ.vue) | Vue | 317 | 2 | 2 | 321 |
| [src/components/Message.vue](/src/components/Message.vue) | Vue | 224 | 4 | 4 | 232 |
| [src/components/Position/Banner.vue](/src/components/Position/Banner.vue) | Vue | 150 | 4 | 3 | 157 |
| [src/components/Position/Enclosure.vue](/src/components/Position/Enclosure.vue) | Vue | 346 | 10 | 9 | 365 |
| [src/components/Position/Index.vue](/src/components/Position/Index.vue) | Vue | 236 | 8 | 13 | 257 |
| [src/components/Position/Position.vue](/src/components/Position/Position.vue) | Vue | 276 | 6 | 7 | 289 |
| [src/components/Position/Setting.vue](/src/components/Position/Setting.vue) | Vue | 136 | 9 | 8 | 153 |
| [src/components/Position/Track.vue](/src/components/Position/Track.vue) | Vue | 414 | 29 | 14 | 457 |
| [src/components/Relay.vue](/src/components/Relay.vue) | Vue | 262 | 2 | 3 | 267 |
| [src/components/RemoteAir/Banner.vue](/src/components/RemoteAir/Banner.vue) | Vue | 171 | 2 | 2 | 175 |
| [src/components/RemotePower/Banner.vue](/src/components/RemotePower/Banner.vue) | Vue | 174 | 2 | 2 | 178 |
| [src/components/Skin.vue](/src/components/Skin.vue) | Vue | 147 | 3 | 2 | 152 |
| [src/components/SmartKit/Banner.vue](/src/components/SmartKit/Banner.vue) | Vue | 156 | 4 | 2 | 162 |
| [src/components/SmartKit/Index.vue](/src/components/SmartKit/Index.vue) | Vue | 382 | 17 | 11 | 410 |
| [src/components/SmartKit/SmartKit.ino](/src/components/SmartKit/SmartKit.ino) | C++ | 381 | 61 | 87 | 529 |
| [src/components/SmartLight.vue](/src/components/SmartLight.vue) | Vue | 182 | 2 | 3 | 187 |
| [src/components/Step.vue](/src/components/Step.vue) | Vue | 179 | 2 | 2 | 183 |
| [src/components/ToolBar/Home.vue](/src/components/ToolBar/Home.vue) | Vue | 378 | 15 | 17 | 410 |
| [src/components/ToolBar/Message.vue](/src/components/ToolBar/Message.vue) | Vue | 91 | 5 | 11 | 107 |
| [src/components/ToolBar/Mine.vue](/src/components/ToolBar/Mine.vue) | Vue | 178 | 13 | 6 | 197 |
| [src/components/ToolBar/Shopping.vue](/src/components/ToolBar/Shopping.vue) | Vue | 162 | 13 | 7 | 182 |
| [src/components/Video.vue](/src/components/Video.vue) | Vue | 49 | 1 | 3 | 53 |
| [src/main.js](/src/main.js) | JavaScript | 48 | 7 | 7 | 62 |
| [src/router/index.js](/src/router/index.js) | JavaScript | 161 | 7 | 5 | 173 |
| [src/views/AddDevice.vue](/src/views/AddDevice.vue) | Vue | 174 | 15 | 6 | 195 |
| [src/views/Home/GroupTab.vue](/src/views/Home/GroupTab.vue) | Vue | 255 | 26 | 6 | 287 |
| [src/views/Home/RoomTab.vue](/src/views/Home/RoomTab.vue) | Vue | 255 | 26 | 6 | 287 |
| [src/views/Login.vue](/src/views/Login.vue) | Vue | 237 | 23 | 7 | 267 |
| [src/views/Main.vue](/src/views/Main.vue) | Vue | 73 | 11 | 3 | 87 |
| [src/views/Mine/Advise.vue](/src/views/Mine/Advise.vue) | Vue | 135 | 9 | 8 | 152 |
| [src/views/Mine/ChangeBg.vue](/src/views/Mine/ChangeBg.vue) | Vue | 189 | 11 | 9 | 209 |
| [src/views/Register.vue](/src/views/Register.vue) | Vue | 206 | 36 | 6 | 248 |
| [src/views/ScanQRCode.vue](/src/views/ScanQRCode.vue) | Vue | 235 | 12 | 18 | 265 |
| [src/views/fishMain.vue](/src/views/fishMain.vue) | Vue | 334 | 12 | 12 | 358 |
| [src/views/lrblMain.vue](/src/views/lrblMain.vue) | Vue | 410 | 16 | 12 | 438 |
| [src/views/skinMain.vue](/src/views/skinMain.vue) | Vue | 382 | 26 | 12 | 420 |
| [src/views/smartLightMain.vue](/src/views/smartLightMain.vue) | Vue | 326 | 11 | 13 | 350 |
| [src/views/stepMain.vue](/src/views/stepMain.vue) | Vue | 465 | 51 | 11 | 527 |
| [src/views/stepSettings.vue](/src/views/stepSettings.vue) | Vue | 234 | 8 | 10 | 252 |
| [src/views/test.vue](/src/views/test.vue) | Vue | 207 | 9 | 13 | 229 |
| [vue.config.js](/vue.config.js) | JavaScript | 23 | 14 | 3 | 40 |

[summary](results.md)