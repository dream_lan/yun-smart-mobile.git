# Summary

Date : 2021-09-17 20:23:28

Directory /Users/mac/Desktop/开发项目/Vue/wsn

Total : 84 files,  27526 codes, 885 comments, 584 blanks, all 28995 lines

[details](details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| JSON | 2 | 13,716 | 0 | 2 | 13,718 |
| Vue | 52 | 12,782 | 715 | 411 | 13,908 |
| JavaScript | 9 | 386 | 28 | 38 | 452 |
| C++ | 1 | 381 | 61 | 87 | 529 |
| SCSS | 3 | 94 | 15 | 15 | 124 |
| Python | 1 | 84 | 13 | 21 | 118 |
| XML | 13 | 37 | 0 | 0 | 37 |
| Markdown | 1 | 19 | 0 | 6 | 25 |
| HTML | 1 | 19 | 1 | 1 | 21 |
| CSS | 1 | 8 | 52 | 3 | 63 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 84 | 27,526 | 885 | 584 | 28,995 |
| public | 1 | 19 | 1 | 1 | 21 |
| python | 1 | 84 | 13 | 21 | 118 |
| src | 76 | 13,640 | 857 | 549 | 15,046 |
| src/api | 2 | 40 | 0 | 2 | 42 |
| src/assets | 19 | 228 | 67 | 37 | 332 |
| src/assets/bg | 2 | 26 | 0 | 0 | 26 |
| src/assets/css | 4 | 102 | 67 | 18 | 187 |
| src/assets/img | 4 | 4 | 0 | 0 | 4 |
| src/assets/js | 2 | 89 | 0 | 19 | 108 |
| src/assets/svg | 7 | 7 | 0 | 0 | 7 |
| src/components | 36 | 8,849 | 431 | 324 | 9,604 |
| src/components/Cat_Home | 2 | 683 | 38 | 29 | 750 |
| src/components/Dorm | 2 | 575 | 16 | 13 | 604 |
| src/components/Farm | 2 | 521 | 24 | 14 | 559 |
| src/components/InterFacePage | 3 | 1,027 | 115 | 31 | 1,173 |
| src/components/Lab_1 | 2 | 423 | 17 | 12 | 452 |
| src/components/Position | 6 | 1,558 | 66 | 54 | 1,678 |
| src/components/RemoteAir | 1 | 171 | 2 | 2 | 175 |
| src/components/RemotePower | 1 | 174 | 2 | 2 | 178 |
| src/components/SmartKit | 3 | 919 | 82 | 100 | 1,101 |
| src/components/ToolBar | 4 | 809 | 46 | 41 | 896 |
| src/router | 1 | 161 | 7 | 5 | 173 |
| src/views | 16 | 4,117 | 302 | 152 | 4,571 |
| src/views/Home | 2 | 510 | 52 | 12 | 574 |
| src/views/Mine | 2 | 324 | 20 | 17 | 361 |

[details](details.md)