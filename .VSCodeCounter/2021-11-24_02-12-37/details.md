# Details

Date : 2021-11-24 02:12:37

Directory /Users/mac/Desktop/开发项目/Vue/EasySmartMobile/src

Total : 101 files,  18588 codes, 1256 comments, 727 blanks, all 20571 lines

[summary](results.md)

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [src/App.vue](/src/App.vue) | Vue | 232 | 60 | 22 | 314 |
| [src/Splash.vue](/src/Splash.vue) | Vue | 40 | 2 | 4 | 46 |
| [src/api/api.js](/src/api/api.js) | JavaScript | 50 | 0 | 7 | 57 |
| [src/api/manage.js](/src/api/manage.js) | JavaScript | 52 | 14 | 4 | 70 |
| [src/api/mqtt.js](/src/api/mqtt.js) | JavaScript | 31 | 0 | 1 | 32 |
| [src/api/mqttcontrol.js](/src/api/mqttcontrol.js) | JavaScript | 9 | 0 | 1 | 10 |
| [src/api/request.js](/src/api/request.js) | JavaScript | 72 | 22 | 7 | 101 |
| [src/assets/bg/bottom.svg](/src/assets/bg/bottom.svg) | XML | 13 | 0 | 0 | 13 |
| [src/assets/bg/top.svg](/src/assets/bg/top.svg) | XML | 13 | 0 | 0 | 13 |
| [src/assets/css/colors.scss](/src/assets/css/colors.scss) | SCSS | 5 | 4 | 1 | 10 |
| [src/assets/css/dimens.scss](/src/assets/css/dimens.scss) | SCSS | 12 | 3 | 2 | 17 |
| [src/assets/css/reset.css](/src/assets/css/reset.css) | CSS | 8 | 52 | 3 | 63 |
| [src/assets/css/style.scss](/src/assets/css/style.scss) | SCSS | 77 | 8 | 12 | 97 |
| [src/assets/img/beat.svg](/src/assets/img/beat.svg) | XML | 1 | 0 | 0 | 1 |
| [src/assets/img/enclosure.svg](/src/assets/img/enclosure.svg) | XML | 1 | 0 | 0 | 1 |
| [src/assets/img/interfacePage_Comment/send_btn.svg](/src/assets/img/interfacePage_Comment/send_btn.svg) | XML | 14 | 0 | 1 | 15 |
| [src/assets/img/position.svg](/src/assets/img/position.svg) | XML | 1 | 0 | 0 | 1 |
| [src/assets/img/track.svg](/src/assets/img/track.svg) | XML | 1 | 0 | 0 | 1 |
| [src/assets/js/calcTextareaHeight.js](/src/assets/js/calcTextareaHeight.js) | JavaScript | 89 | 0 | 15 | 104 |
| [src/assets/js/gloab.js](/src/assets/js/gloab.js) | JavaScript | 6 | 0 | 0 | 6 |
| [src/assets/js/md5.min.js](/src/assets/js/md5.min.js) | JavaScript | 83 | 0 | 19 | 102 |
| [src/assets/svg/485.svg](/src/assets/svg/485.svg) | XML | 1 | 0 | 0 | 1 |
| [src/assets/svg/beat.svg](/src/assets/svg/beat.svg) | XML | 1 | 0 | 0 | 1 |
| [src/assets/svg/device.svg](/src/assets/svg/device.svg) | XML | 1 | 0 | 0 | 1 |
| [src/assets/svg/humi.svg](/src/assets/svg/humi.svg) | XML | 1 | 0 | 0 | 1 |
| [src/assets/svg/id.svg](/src/assets/svg/id.svg) | XML | 1 | 0 | 0 | 1 |
| [src/assets/svg/mode.svg](/src/assets/svg/mode.svg) | XML | 1 | 0 | 0 | 1 |
| [src/assets/svg/send_btn.svg](/src/assets/svg/send_btn.svg) | XML | 14 | 0 | 1 | 15 |
| [src/assets/svg/set.svg](/src/assets/svg/set.svg) | XML | 1 | 0 | 0 | 1 |
| [src/assets/svg/temp.svg](/src/assets/svg/temp.svg) | XML | 1 | 0 | 0 | 1 |
| [src/components/Cat_Home/Banner.vue](/src/components/Cat_Home/Banner.vue) | Vue | 150 | 4 | 2 | 156 |
| [src/components/Cat_Home/Index.vue](/src/components/Cat_Home/Index.vue) | Vue | 533 | 34 | 27 | 594 |
| [src/components/Dorm/Banner.vue](/src/components/Dorm/Banner.vue) | Vue | 150 | 4 | 2 | 156 |
| [src/components/Dorm/Index.vue](/src/components/Dorm/Index.vue) | Vue | 425 | 12 | 11 | 448 |
| [src/components/Farm/Banner.vue](/src/components/Farm/Banner.vue) | Vue | 170 | 2 | 3 | 175 |
| [src/components/Farm/Index.vue](/src/components/Farm/Index.vue) | Vue | 351 | 22 | 11 | 384 |
| [src/components/Fish.vue](/src/components/Fish.vue) | Vue | 248 | 2 | 2 | 252 |
| [src/components/InterFacePage/Banner.vue](/src/components/InterFacePage/Banner.vue) | Vue | 329 | 77 | 8 | 414 |
| [src/components/InterFacePage/Index.vue](/src/components/InterFacePage/Index.vue) | Vue | 447 | 21 | 8 | 476 |
| [src/components/InterFacePage/Info.vue](/src/components/InterFacePage/Info.vue) | Vue | 286 | 22 | 12 | 320 |
| [src/components/InterFacePage/Info/Comment.vue](/src/components/InterFacePage/Info/Comment.vue) | Vue | 284 | 12 | 9 | 305 |
| [src/components/InterFacePage/Info/Settings.vue](/src/components/InterFacePage/Info/Settings.vue) | Vue | 212 | 20 | 12 | 244 |
| [src/components/LRBL.vue](/src/components/LRBL.vue) | Vue | 142 | 3 | 3 | 148 |
| [src/components/Lab_1/Banner.vue](/src/components/Lab_1/Banner.vue) | Vue | 149 | 4 | 2 | 155 |
| [src/components/Lab_1/Index.vue](/src/components/Lab_1/Index.vue) | Vue | 274 | 13 | 10 | 297 |
| [src/components/Light.vue](/src/components/Light.vue) | Vue | 239 | 2 | 2 | 243 |
| [src/components/MQ.vue](/src/components/MQ.vue) | Vue | 317 | 2 | 2 | 321 |
| [src/components/Message.vue](/src/components/Message.vue) | Vue | 224 | 4 | 4 | 232 |
| [src/components/Position/Banner.vue](/src/components/Position/Banner.vue) | Vue | 150 | 4 | 3 | 157 |
| [src/components/Position/Enclosure.vue](/src/components/Position/Enclosure.vue) | Vue | 346 | 10 | 9 | 365 |
| [src/components/Position/Index.vue](/src/components/Position/Index.vue) | Vue | 236 | 8 | 13 | 257 |
| [src/components/Position/Position.vue](/src/components/Position/Position.vue) | Vue | 276 | 6 | 7 | 289 |
| [src/components/Position/Setting.vue](/src/components/Position/Setting.vue) | Vue | 136 | 9 | 8 | 153 |
| [src/components/Position/Track.vue](/src/components/Position/Track.vue) | Vue | 414 | 29 | 14 | 457 |
| [src/components/Position2/Banner.vue](/src/components/Position2/Banner.vue) | Vue | 154 | 4 | 3 | 161 |
| [src/components/Position2/Enclosure.vue](/src/components/Position2/Enclosure.vue) | Vue | 346 | 10 | 9 | 365 |
| [src/components/Position2/Index.vue](/src/components/Position2/Index.vue) | Vue | 236 | 8 | 13 | 257 |
| [src/components/Position2/Position.vue](/src/components/Position2/Position.vue) | Vue | 255 | 21 | 8 | 284 |
| [src/components/Position2/Setting.vue](/src/components/Position2/Setting.vue) | Vue | 136 | 9 | 8 | 153 |
| [src/components/Position2/Track.vue](/src/components/Position2/Track.vue) | Vue | 414 | 29 | 14 | 457 |
| [src/components/Relay.vue](/src/components/Relay.vue) | Vue | 262 | 2 | 3 | 267 |
| [src/components/RemoteAir/Banner.vue](/src/components/RemoteAir/Banner.vue) | Vue | 171 | 2 | 2 | 175 |
| [src/components/RemotePower/Banner.vue](/src/components/RemotePower/Banner.vue) | Vue | 174 | 2 | 2 | 178 |
| [src/components/Skin.vue](/src/components/Skin.vue) | Vue | 147 | 3 | 2 | 152 |
| [src/components/SmartKit/Banner.vue](/src/components/SmartKit/Banner.vue) | Vue | 156 | 4 | 2 | 162 |
| [src/components/SmartKit/Index.vue](/src/components/SmartKit/Index.vue) | Vue | 382 | 17 | 11 | 410 |
| [src/components/SmartKit/SmartKit.ino](/src/components/SmartKit/SmartKit.ino) | C++ | 381 | 61 | 87 | 529 |
| [src/components/SmartLight.vue](/src/components/SmartLight.vue) | Vue | 182 | 2 | 3 | 187 |
| [src/components/Step.vue](/src/components/Step.vue) | Vue | 179 | 2 | 2 | 183 |
| [src/components/ToolBar/Home.vue](/src/components/ToolBar/Home.vue) | Vue | 411 | 16 | 14 | 441 |
| [src/components/ToolBar/Message.vue](/src/components/ToolBar/Message.vue) | Vue | 316 | 15 | 16 | 347 |
| [src/components/ToolBar/Mine.vue](/src/components/ToolBar/Mine.vue) | Vue | 315 | 23 | 7 | 345 |
| [src/components/ToolBar/Shopping.vue](/src/components/ToolBar/Shopping.vue) | Vue | 162 | 13 | 7 | 182 |
| [src/components/Video.vue](/src/components/Video.vue) | Vue | 49 | 1 | 3 | 53 |
| [src/components/cDevice1/Banner.vue](/src/components/cDevice1/Banner.vue) | Vue | 263 | 4 | 10 | 277 |
| [src/components/cDevice1/Index.vue](/src/components/cDevice1/Index.vue) | Vue | 545 | 44 | 17 | 606 |
| [src/components/currency/NavBarWithTab.vue](/src/components/currency/NavBarWithTab.vue) | Vue | 168 | 6 | 3 | 177 |
| [src/main.js](/src/main.js) | JavaScript | 56 | 8 | 6 | 70 |
| [src/router/index.js](/src/router/index.js) | JavaScript | 226 | 7 | 8 | 241 |
| [src/store/index.js](/src/store/index.js) | JavaScript | 23 | 1 | 2 | 26 |
| [src/views/AddDevice.vue](/src/views/AddDevice.vue) | Vue | 174 | 15 | 6 | 195 |
| [src/views/Home/GroupTab.vue](/src/views/Home/GroupTab.vue) | Vue | 255 | 26 | 6 | 287 |
| [src/views/Home/RoomTab.vue](/src/views/Home/RoomTab.vue) | Vue | 255 | 26 | 6 | 287 |
| [src/views/Login.vue](/src/views/Login.vue) | Vue | 239 | 23 | 6 | 268 |
| [src/views/Main.vue](/src/views/Main.vue) | Vue | 129 | 69 | 6 | 204 |
| [src/views/Mine/Advise.vue](/src/views/Mine/Advise.vue) | Vue | 142 | 3 | 8 | 153 |
| [src/views/Mine/Center.vue](/src/views/Mine/Center.vue) | Vue | 289 | 42 | 10 | 341 |
| [src/views/Mine/ChangeBg.vue](/src/views/Mine/ChangeBg.vue) | Vue | 196 | 5 | 9 | 210 |
| [src/views/Mine/Friends.vue](/src/views/Mine/Friends.vue) | Vue | 40 | 2 | 4 | 46 |
| [src/views/Mine/updateUserName.vue](/src/views/Mine/updateUserName.vue) | Vue | 68 | 0 | 3 | 71 |
| [src/views/Register.vue](/src/views/Register.vue) | Vue | 294 | 38 | 5 | 337 |
| [src/views/Register2.vue](/src/views/Register2.vue) | Vue | 230 | 37 | 5 | 272 |
| [src/views/ResetPasswd.vue](/src/views/ResetPasswd.vue) | Vue | 255 | 13 | 5 | 273 |
| [src/views/ScanQRCode.vue](/src/views/ScanQRCode.vue) | Vue | 235 | 19 | 18 | 272 |
| [src/views/fishMain.vue](/src/views/fishMain.vue) | Vue | 334 | 12 | 12 | 358 |
| [src/views/lrblMain.vue](/src/views/lrblMain.vue) | Vue | 410 | 16 | 12 | 438 |
| [src/views/skinMain.vue](/src/views/skinMain.vue) | Vue | 382 | 26 | 12 | 420 |
| [src/views/smartLightMain.vue](/src/views/smartLightMain.vue) | Vue | 326 | 11 | 13 | 350 |
| [src/views/stepMain.vue](/src/views/stepMain.vue) | Vue | 465 | 51 | 11 | 527 |
| [src/views/stepSettings.vue](/src/views/stepSettings.vue) | Vue | 234 | 8 | 10 | 252 |
| [src/views/test.vue](/src/views/test.vue) | Vue | 207 | 9 | 14 | 230 |

[summary](results.md)