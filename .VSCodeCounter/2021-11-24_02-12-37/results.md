# Summary

Date : 2021-11-24 02:12:37

Directory /Users/mac/Desktop/开发项目/Vue/EasySmartMobile/src

Total : 101 files,  18588 codes, 1256 comments, 727 blanks, all 20571 lines

[details](details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| Vue | 69 | 17,342 | 1,076 | 550 | 18,968 |
| JavaScript | 11 | 697 | 52 | 70 | 819 |
| C++ | 1 | 381 | 61 | 87 | 529 |
| SCSS | 3 | 94 | 15 | 15 | 124 |
| XML | 16 | 66 | 0 | 2 | 68 |
| CSS | 1 | 8 | 52 | 3 | 63 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 101 | 18,588 | 1,256 | 727 | 20,571 |
| api | 5 | 214 | 36 | 20 | 270 |
| assets | 23 | 346 | 67 | 54 | 467 |
| assets/bg | 2 | 26 | 0 | 0 | 26 |
| assets/css | 4 | 102 | 67 | 18 | 187 |
| assets/img | 5 | 18 | 0 | 1 | 19 |
| assets/img/interfacePage_Comment | 1 | 14 | 0 | 1 | 15 |
| assets/js | 3 | 178 | 0 | 34 | 212 |
| assets/svg | 9 | 22 | 0 | 1 | 23 |
| components | 47 | 12,292 | 624 | 430 | 13,346 |
| components/Cat_Home | 2 | 683 | 38 | 29 | 750 |
| components/Dorm | 2 | 575 | 16 | 13 | 604 |
| components/Farm | 2 | 521 | 24 | 14 | 559 |
| components/InterFacePage | 5 | 1,558 | 152 | 49 | 1,759 |
| components/InterFacePage/Info | 2 | 496 | 32 | 21 | 549 |
| components/Lab_1 | 2 | 423 | 17 | 12 | 452 |
| components/Position | 6 | 1,558 | 66 | 54 | 1,678 |
| components/Position2 | 6 | 1,541 | 81 | 55 | 1,677 |
| components/RemoteAir | 1 | 171 | 2 | 2 | 175 |
| components/RemotePower | 1 | 174 | 2 | 2 | 178 |
| components/SmartKit | 3 | 919 | 82 | 100 | 1,101 |
| components/ToolBar | 4 | 1,204 | 67 | 44 | 1,315 |
| components/cDevice1 | 2 | 808 | 48 | 27 | 883 |
| components/currency | 1 | 168 | 6 | 3 | 177 |
| router | 1 | 226 | 7 | 8 | 241 |
| store | 1 | 23 | 1 | 2 | 26 |
| views | 21 | 5,159 | 451 | 181 | 5,791 |
| views/Home | 2 | 510 | 52 | 12 | 574 |
| views/Mine | 5 | 735 | 52 | 34 | 821 |

[details](details.md)