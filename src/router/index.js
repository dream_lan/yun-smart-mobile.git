
import Vue from 'vue';
import { PasswordInput, NumberKeyboard } from 'vant';

Vue.use(PasswordInput);
Vue.use(NumberKeyboard);

import Router from 'vue-router'
//import Main from '../views/Main.vue'

Vue.use(Router)
let router = new Router({
  mode: 'hash',
  //base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'Main',
      component: () => import(/* webpackChunkName: "about" */ '../views/Main.vue')
    },
    {
      path: '/test',
      name: 'test',
      component: () => import(/* webpackChunkName: "about" */ '../views/test.vue')
    },
    {
      path: '/scanqrcode',
      name: 'scanqrcode',
      component: () => import(/* webpackChunkName: "about" */ '../views/ScanQRCode.vue')
    },
    {
      path: '/changebg',
      name: 'changebg',
      component: () => import(/* webpackChunkName: "about" */ '../views/Mine/ChangeBg.vue')
    },
    {
      path: '/friends',
      name: 'friends',
      component: () => import(/* webpackChunkName: "about" */ '../views/Mine/Friends.vue')
    },
    {
      path: '/center',
      name: 'center',
      component: () => import(/* webpackChunkName: "about" */ '../views/Mine/Center.vue')
    },
    {
      path: '/advise',
      name: 'advise',
      component: () => import(/* webpackChunkName: "about" */ '../views/Mine/Advise.vue')
    },
    {
      path: '/login',
      name: 'login',
      component: () => import(/* webpackChunkName: "about" */ '../views/Login.vue')
    },
    {
      path: '/register',
      name: 'register',
      component: () => import(/* webpackChunkName: "about" */ '../views/Register.vue')
    },
    {
      path: '/register2',
      name: 'register2',
      component: () => import(/* webpackChunkName: "about" */ '../views/Register2.vue')
    },
    {
      path: '/resetpasswd',
      name: 'resetpasswd',
      component: () => import(/* webpackChunkName: "about" */ '../views/ResetPasswd.vue')
    },
    {
      path: '/addDevice',
      name: 'adddevice',
      component: () => import(/* webpackChunkName: "about" */ '../views/AddDevice.vue')
    },
    {
      path:'/updateUserName',
      name:'updateUserName',
      component: () => import(/* webpackChunkName: "about" */ '../views/Mine/updateUserName.vue')
    },
    {
      path: '/custom_page_index',
      name: 'custom_page_index',
      component: () => import(/* webpackChunkName: "about" */ '../components/CustomPage/Index.vue')
    },
    {
      path: '/interface_page_index',
      name: 'interface_page_index',
      component: () => import(/* webpackChunkName: "about" */ '../components/InterFacePage/Index.vue')
    },
    {
      path: '/interface_page_info',
      name: 'interface_page_info',
      component: () => import(/* webpackChunkName: "about" */ '../components/InterFacePage/Info.vue')
    },
    {
      path: '/interface_page_comment',
      name: 'interface_page_comment',
      component: () => import(/* webpackChunkName: "about" */ '../components/InterFacePage/Info/Comment.vue')
    },
    {
      path: '/interface_page_settings',
      name: 'interface_page_settings',
      component: () => import(/* webpackChunkName: "about" */ '../components/InterFacePage/Info/Settings.vue')
    },
    {
      path: '/fishmain',
      name: 'fishMain',
      component: () => import(/* webpackChunkName: "about" */ '../components/CustomPages/Fish/fishMain.vue')
    },
    {
      path: '/smartlightmain',
      name: 'smartLightMain',
      component: () => import(/* webpackChunkName: "about" */ '../components/CustomPages/SmartLight/smartLightMain.vue')
    },
    {
      path: '/stepmain',
      name: 'stepMain',
      component: () => import(/* webpackChunkName: "about" */ '../components/CustomPages/Step/stepMain.vue')
    },
    {
      path: '/skinmain',
      name: 'skinMain',
      component: () => import(/* webpackChunkName: "about" */ '../components/CustomPages/Skin/skinMain.vue')
    },
    {
      path: '/lrblmain',
      name: 'lrblMain',
      component: () => import(/* webpackChunkName: "about" */ '../components/CustomPages/LRBL/lrblMain.vue')
    },
    {
      path: '/farm_index',
      name: 'farm_index',
      component: () => import(/* webpackChunkName: "about" */ '../components/Farm/Index.vue')
    },
    {
      path: '/stepsettings',
      name: 'stepSettings',
      component: () => import(/* webpackChunkName: "about" */ '../components/CustomPages/Step/stepSettings.vue')
    },
    {
      path: '/lab_1_index',
      name: 'lab_1_index',
      component: () => import(/* webpackChunkName: "about" */ '../components/Lab_1/Index.vue')
    },
    {
      path: '/dorm',
      name: 'dorm_index',
      component: () => import(/* webpackChunkName: "about" */ '../components/Dorm/Index.vue')
    },
    {
      path: '/smartkit',
      name: 'smartkit_index',
      component: () => import(/* webpackChunkName: "about" */ '../components/SmartKit/Index.vue')
    },
    {
      path: '/position',
      name: 'position_index',
      component: () => import(/* webpackChunkName: "about" */ '../components/Position/Index.vue')
    },
    {
      path: '/position_position',
      name: 'position_position',
      component: () => import(/* webpackChunkName: "about" */ '../components/Position/Position.vue')
    },
    {
      path: '/position_track',
      name: 'position_track',
      component: () => import(/* webpackChunkName: "about" */ '../components/Position/Track.vue')
    },
    {
      path: '/position_enclosure',
      name: 'position_enclosure',
      component: () => import(/* webpackChunkName: "about" */ '../components/Position/Enclosure.vue')
    },
    {
      path: '/position_setting',
      name: 'position_setting',
      component: () => import(/* webpackChunkName: "about" */ '../components/Position/Setting.vue')
    },
    {
      path: '/express1',
      name: 'express1_index',
      component: () => import(/* webpackChunkName: "about" */ '../components/Express1/Index.vue')
    },
    {
      path: '/express1_position',
      name: 'express1_position',
      component: () => import(/* webpackChunkName: "about" */ '../components/Express1/Position.vue')
    },
    {
      path: '/position2',
      name: 'position2_index',
      component: () => import(/* webpackChunkName: "about" */ '../components/Position2/Index.vue')
    },
    {
      path: '/position2_position',
      name: 'position2_position',
      component: () => import(/* webpackChunkName: "about" */ '../components/Position2/Position.vue')
    },
    {
      path: '/position2_track',
      name: 'position2_track',
      component: () => import(/* webpackChunkName: "about" */ '../components/Position2/Track.vue')
    },
    {
      path: '/position2_enclosure',
      name: 'position2_enclosure',
      component: () => import(/* webpackChunkName: "about" */ '../components/Position2/Enclosure.vue')
    },
    {
      path: '/position2_setting',
      name: 'position2_setting',
      component: () => import(/* webpackChunkName: "about" */ '../components/Position2/Setting.vue')
    },

    {
      path: '/FailDetect_1',
      name: 'FailDetect_1_index',
      component: () => import(/* webpackChunkName: "about" */ '../components/FailDetect_1/Index.vue')
    },
    {
      path: '/FailDetect_1_position',
      name: 'FailDetect_1_position',
      component: () => import(/* webpackChunkName: "about" */ '../components/FailDetect_1/Position.vue')
    },
    {
      path: '/FailDetect_1_track',
      name: 'FailDetect_1_track',
      component: () => import(/* webpackChunkName: "about" */ '../components/FailDetect_1/Track.vue')
    },

    {
      path: '/cat_home_index',
      name: 'cat_home_index',
      component: () => import(/* webpackChunkName: "about" */ '../components/Cat_Home/Index.vue')
    },
    {
      path: '/cdevice1_index',
      name: 'cdevice1_index',
      component: () => import(/* webpackChunkName: "about" */ '../components/cDevice1/Index.vue')
    },
    {
      path: '/Farm_2',
      name: 'Farm_2_index',
      component: () => import(/* webpackChunkName: "about" */ '../components/Farm_2/Index.vue')
    },
    {
      path: '/Farm_2',
      name: 'Farm_2_logs',
      component: () => import(/* webpackChunkName: "about" */ '../components/Farm_2/Logs.vue')
    },
    {
      path: '/Farm_2',
      name: 'Farm_2_control',
      component: () => import(/* webpackChunkName: "about" */ '../components/Farm_2/Control.vue')
    },
    {
      path: '/Farm_2',
      name: 'Farm_2_main',
      component: () => import(/* webpackChunkName: "about" */ '../components/Farm_2/Main.vue')
    },
    {
      path: '/Farm_2',
      name: 'Farm_2_setting',
      component: () => import(/* webpackChunkName: "about" */ '../components/Farm_2/Setting.vue')
    },
    {
      path: '/MircoStep_1',
      name: 'MircoStep_1_index',
      component: () => import(/* webpackChunkName: "about" */ '../components/MircoStep_1/Index.vue')
    },

    {
      path: '/LYCWDW',
      name: 'LYCWDW_index',
      component: () => import(/* webpackChunkName: "about" */ '../components/LYCWDW/Index.vue')
    },
    {
      path: '/LYCWDW_position',
      name: 'LYCWDW_position',
      component: () => import(/* webpackChunkName: "LYCWDW" */ '../components/LYCWDW/Position.vue')
    },
    {
      path: '/LYCWDW_track',
      name: 'LYCWDW_track',
      component: () => import(/* webpackChunkName: "LYCWDW" */ '../components/LYCWDW/Track.vue')
    },
    {
      path: '/LYCWDW_enclosure',
      name: 'LYCWDW_enclosure',
      component: () => import(/* webpackChunkName: "LYCWDW" */ '../components/LYCWDW/Enclosure.vue')
    },
    {
      path: '/LYCWDW_setting',
      name: 'LYCWDW_setting',
      component: () => import(/* webpackChunkName: "LYCWDW" */ '../components/LYCWDW/Setting.vue')
    },

    {
      path: '/LMGETDY',
      name: 'LMGETDY_index',
      component: () => import(/* webpackChunkName: "LMGETDY" */ '../components/CustomPages/LMGETDY/Index.vue')
    },
    {
      path: '/LMGETDY_position',
      name: 'LMGETDY_position',
      component: () => import(/* webpackChunkName: "LMGETDY" */ '../components/CustomPages/LMGETDY/Position.vue')
    },
    {
      path: '/LMGETDY_track',
      name: 'LMGETDY_track',
      component: () => import(/* webpackChunkName: "LMGETDY" */ '../components/CustomPages/LMGETDY/Track.vue')
    },
    {
      path: '/LMGETDY_enclosure',
      name: 'LMGETDY_enclosure',
      component: () => import(/* webpackChunkName: "LMGETDY" */ '../components/CustomPages/LMGETDY/Enclosure.vue')
    },
    {
      path: '/LMGETDY_setting',
      name: 'LMGETDY_setting',
      component: () => import(/* webpackChunkName: "LMGETDY" */ '../components/CustomPages/LMGETDY/Setting.vue')
    },

    {
      path: '/LXNCDWDW',
      name: 'LXNCDWDW_index',
      component: () => import(/* webpackChunkName: "LXNCDWDW" */ '../components/CustomPages/LXNCDWDW/Index')
    },
    {
      path: '/LXNCDWDW_position',
      name: 'LXNCDWDW_position',
      component: () => import(/* webpackChunkName: "LXNCDWDW" */ '../components/CustomPages/LXNCDWDW/Position.vue')
    },
    {
      path: '/LXNCDWDW_track',
      name: 'LXNCDWDW_track',
      component: () => import(/* webpackChunkName: "LXNCDWDW" */ '../components/CustomPages/LXNCDWDW/Track.vue')
    },
    {
      path: '/LXNCDWDW_interface',
      name: 'LXNCDWDW_interface',
      component: () => import(/* webpackChunkName: "LXNCDWDW" */ '../components/CustomPages/LXNCDWDW/InterFace.vue')
    },
  ]
})
export default router

router.beforeEach((to, from, next) => {
  let islogin = localStorage.getItem("islogin");
  // console.log(islogin);
  // console.log(to.path);
  //islogin = false
  islogin = Boolean(Number(islogin));
  console.log(islogin)
  //next("/login")
  if(to.path == "/login" || to.path == "/register"|| to.path == "/register2"|| to.path == "/resetpasswd"){
    if(islogin){
      next("/");
    }else{
      next();
    }
  }
  else{
    // requireAuth:可以在路由元信息指定哪些页面需要登录权限
    if(islogin) {
      next();
    }else{
      next("/login");
    }
  }
})