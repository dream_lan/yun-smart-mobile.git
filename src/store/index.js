import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    userAvatar: "",
    nickName:"",
    deviceNumber:"",
    deviceAddr:"--"
  },
  mutations: {
    // 保存当前菜单栏的路径
    saveUserAvatar(state, res) {
      state.userAvatar = res;
    },
    saveNickName(state, res) {
      state.nickName = res;
    },
    saveDeviceNumber(state, res) {
      state.deviceNumber = res;
    },
    saveGPSAddr(state, res) {
      state.deviceAddr = res;
    },
  },
  actions: {},
  modules: {}
})