local cjson = require "cjson"
local deviceId=100015
local version="VER22-01-160"
-- 界面0：选择继电器【记录】
local shifei_select = 0;
local dayao_select = 0;
local shuibeng_select = 0;

-- 施肥、打药、水泵工作时间
local shifei_time = 0;
local shifei_time_sec = 0;
local dayao_time = 0;
local dayao_time_sec = 0;
local shuibeng_shifei_time = 0;
local shuibeng_shifei_time_sec = 0;
local shuibeng_dayao_time = 0;
local shuibeng_dayao_time_sec = 0;

-- 定时器计数
local shifei_timer_count = 0;
local shifei_timer_count_sec = 0;
local dayao_timer_count = 0;
local dayao_timer_count_sec = 0;
local shuibeng_shifei_timer_count = 0;
local shuibeng_shifei_timer_count_sec = 0;
local shuibeng_dayao_timer_count = 0;
local shuibeng_dayao_timer_count_sec = 0;

-- 查询在线情况的队列
local query_queue = {0}
query_queue["num"] = 0
-- 查询状态的队列
local query_status = {0}
query_status["num"] = 0
-- 打开继电器的队列
local open_group_queue = {0}
local open_name_queue = {0}
open_group_queue["num"] = 0
open_name_queue["num"] = 0
-- 关闭继电器的队列
local close_group_queue = {0}
local close_name_queue = {0}
close_group_queue["num"] = 0
close_name_queue["num"] = 0

-- 添加删除记录选中项
local delete_record_value = 0

-- 设备是否在工作
local shifei_isWorking = 0
local dayao_isWorking = 0
local shuibeng_shifei_isWorking = 0
local shuibeng_dayao_isWorking = 0

-- 延时控制
local open_delay_count = 0
local close_delay_count = 0
local query_delay_count = 0

-- 开机初始化
function on_init()
    -- 初始化串口发送定时器
	start_timer(4,5000,1,0)
	print("初始化定时器")
	--显示版本号
	set_text(1,23,version)
    -- 隐藏画面0设备列表
    set_visiable(0, 39, 0)
    set_visiable(0, 45, 0)
    set_visiable(0, 50, 0)
    -- 读取保存的时间设定值
    local shifei_setTime = read_flash_string(30000)
    local shifei_setTime_sec = read_flash_string(30001)
    local dayao_setTime = read_flash_string(30010)
    local dayao_setTime_sec = read_flash_string(30011)
    local shuibeng_shifei_setTime = read_flash_string(30020)
    local shuibeng_shifei_setTime_sec = read_flash_string(30021)
    local shuibeng_dayao_setTime = read_flash_string(30030)
    local shuibeng_dayao_setTime_sec = read_flash_string(30031)
    if shifei_setTime ~= nil then
        set_text(1, 1, shifei_setTime)
        shifei_time = tonumber(shifei_setTime)
        print('shifei_time:'..shifei_time)
    end
    if shifei_setTime_sec ~= nil then
        set_text(1, 36, shifei_setTime_sec)
        shifei_time_sec = tonumber(shifei_setTime_sec)
        print('shifei_time_sec:'..shifei_time_sec)
    end
    if dayao_setTime ~= nil then
        set_text(1, 7, dayao_setTime)
        dayao_time = tonumber(dayao_setTime)
        print('dayao_time:'..dayao_time)
    end
    if dayao_setTime_sec ~= nil then
        set_text(1, 37, dayao_setTime_sec)
        dayao_time_sec = tonumber(dayao_setTime_sec)
        print('dayao_time_sec:'..dayao_time_sec)
    end
    if shuibeng_shifei_setTime ~= nil then
        set_text(1, 12, shuibeng_shifei_setTime)
        shuibeng_shifei_time = tonumber(shuibeng_shifei_setTime)
        print('shuibeng_shifei_time:'..shuibeng_shifei_time)
    end
    if shuibeng_shifei_setTime_sec ~= nil then
        set_text(1, 38, shuibeng_shifei_setTime_sec)
        shuibeng_shifei_time_sec = tonumber(shuibeng_shifei_setTime_sec)
        print('shuibeng_shifei_time_sec:'..shuibeng_shifei_time_sec)
    end
    if shuibeng_dayao_setTime ~= nil then
        set_text(1, 25, shuibeng_dayao_setTime)
        shuibeng_dayao_time = tonumber(shuibeng_dayao_setTime)
        print('shuibeng_dayao_time:'..shuibeng_dayao_time)
    end
    if shuibeng_dayao_setTime_sec ~= nil then
        set_text(1, 39, shuibeng_dayao_setTime_sec)
        shuibeng_dayao_time_sec = tonumber(shuibeng_dayao_setTime_sec)
        print('shuibeng_dayao_time_sec:'..shuibeng_dayao_time_sec)
    end
end

-- 1s自动调用
local init_done = 0 -- 初始化完成
local init_doing = 0 -- 正在初始化
local check_online_count = 0
local net_status = 0 -- 网络连接状态
local all_devices = 0 -- 设备总数量
local init_device_count = 0 -- 已经初始化设备的计数
local init_done_delay = 3 -- 初始化完成后的延时
--每秒调用一次
function on_systick()
    
    -- 获取网络状态
    net_status = get_network_state()
    local dhcp, ipaddr, netmask, gateway, dns = get_network_cfg()
    set_text(1, 21, ipaddr)
    if net_status == 0 then
        set_text(1, 27, "未连接")
    elseif net_status == 1 then
        set_text(1, 27, "已连接")
    else
        set_text(1, 27, net_status)
    end

    -- 连上网后的初始化
    if init_done == 0 and net_status == 1 then
    --if init_done == 0 then
        -- 如果是首次初始化
        if init_done == 0 and init_doing == 0 then
            init_doing = 1
            check_online_count = 0
            -- 获取继电器设备总数量
            all_devices = 0
            local cur_ip = '000'
            if record_get_count(2, 60) ~= 0 then
                for i = 0, record_get_count(2, 60) - 1 do
                    local cur_read_record = record_read(2,60,i)
                    if string.match(cur_read_record,'%d+') ~= cur_ip then
                        cur_ip = string.match(cur_read_record,'%d+')
                        all_devices = all_devices + 1
                    end
                end
            end
            if record_get_count(2, 4) ~= 0 then
                for i = 0, record_get_count(2, 4) - 1 do
                    local cur_read_record = record_read(2,4,i)
                    if string.match(cur_read_record,'%d+') ~= cur_ip then
                        cur_ip = string.match(cur_read_record,'%d+')
                        all_devices = all_devices + 1
                    end
                end
            end
            if record_get_count(2, 7) ~= 0 then
                for i = 0, record_get_count(2, 7) - 1 do
                    local cur_read_record = record_read(2,7,i)
                    if string.match(cur_read_record,'%d+') ~= cur_ip then
                        cur_ip = string.match(cur_read_record,'%d+')
                        all_devices = all_devices + 1
                    end
                end
            end
            set_text(4,3,'设备数：'..tostring(all_devices))

            -- 开始检查设备是否在线
            check_status()
            -- 检查工作区继电器是否都还在
            if record_get_count(0, 60) ~= 0 then
                for i = 0, record_get_count(0, 60) - 1 do
                    local cur_read_record = record_read(0,60,i)
                    local name = string.match(cur_read_record, "施肥%d+")
                    local count=0
                    if record_get_count(2, 60) ~= 0 then
                        for j = 0, record_get_count(2, 60) - 1 do
                            local cur_read_record1 = record_read(2,60,j)
                            if string.find(cur_read_record1, name, 1) ~= nil then
                                count=1
                            end
                        end
                    end
                    if count == 0 then
                        record_delete(0,60,i)
                        i=0
                    end
                end
            end
            if record_get_count(0, 19) ~= 0 then
                for i = 0, record_get_count(0, 19) - 1 do
                    local cur_read_record = record_read(0,19,i)
                    local name = string.match(cur_read_record, "打药%d+")
                    local count=0
                    if record_get_count(2, 4) ~= 0 then
                        for j = 0, record_get_count(2, 4) - 1 do
                            local cur_read_record1 = record_read(2,4,j)
                            if string.find(cur_read_record1, name, 1) ~= nil then
                                count=1
                            end
                        end
                    end
                    if count == 0 then
                        record_delete(0,19,i)
                        i=0
                    end
                end
            end
            if record_get_count(0, 23) ~= 0 then
                for i = 0, record_get_count(0, 23) - 1 do
                    local cur_read_record = record_read(0,23,i)
 					-- local name 	= "";
					-- if cur_read_record == 1
					-- 	then
					-- 		name = "增压水泵"
					-- elseif cur_read_record == 2
					-- 	then 
					-- 		name = "搅拌电机"
 					-- elseif cur_read_record == 3
					-- 	then 
					-- 		name = "主水阀1"
					-- elseif cur_read_record == 4
					-- 	then 
					-- 		name = "主水阀2"	
					-- 	end

                    local name = string.match(cur_read_record, "水泵施肥%d+")
                    local count=0
                    if record_get_count(2, 7) ~= 0 then
                        for j = 0, record_get_count(2, 7) - 1 do
                            local cur_read_record1 = record_read(2,7,j)
                            if string.find(cur_read_record1, name, 1) ~= nil then
                                count=1
                            end
                        end
                    end
                    if count == 0 then
                        record_delete(0,23,i)
                        i=0
                    end
                end
            end
            if record_get_count(0, 36) ~= 0 then
                for i = 0, record_get_count(0, 36) - 1 do
                    local cur_read_record = record_read(0,36,i)
                    local name = string.match(cur_read_record, "水泵打药%d+")
                    local count=0
                    if record_get_count(2, 7) ~= 0 then
                        for j = 0, record_get_count(2, 7) - 1 do
                            local cur_read_record1 = record_read(2,7,j)
                            if string.find(cur_read_record1, name, 1) ~= nil then
                                count=1
                            end
                        end
                    end
                    if count == 0 then
                        record_delete(0,36,i)
                        i=0
                    end
                end
            end
        else
            set_text(4,4,'已初始化:'..tostring(init_device_count))
            if init_device_count >= all_devices then
                init_done_delay = init_done_delay - 1  -- 延时等待最后一个设备的在线状态
                if init_done_delay == 0 then
                    init_done = 1
                    init_doing = 0

                    -- 判断是否有离线设备
                    if record_get_count(2, 60) ~= 0 then
                        for i = 0, record_get_count(2, 60) - 1 do
                            local cur_record_read = record_read(2,60,i)
                            if string.find(cur_record_read,'离',1) ~= nil then
                                change_screen(7)
                                return
                            end
                        end
                    end
                    if record_get_count(2, 4) ~= 0 then
                        for i = 0, record_get_count(2, 4) - 1 do
                            local cur_record_read = record_read(2,4,i)
                            if string.find(cur_record_read,'离',1) ~= nil then
                                change_screen(7)
                                return
                            end
                        end
                    end
                    if record_get_count(2, 7) ~= 0 then
                        for i = 0, record_get_count(2, 7) - 1 do
                            local cur_record_read = record_read(2,7,i)
                            if string.find(cur_record_read,'离',1) ~= nil then
                                change_screen(7)
                                return
                            end
                        end
                    end
                    change_screen(0)
                end
            end
        end
    end

    -- 关闭继电器
    if close_name_queue["num"] ~= 0 then
        -- 清除其他任务计时
        query_delay_count = 0
        check_online_count = 0

        close_delay_count = close_delay_count + 1
        if close_delay_count >= 2 then
            close_delay_count = 0
            close_group = out_queue(close_group_queue)
            close_name = out_queue(close_name_queue)
            close_mydevice()
            return
        end
    else
        close_delay_count = 0
    end

    -- 打开继电器
    if open_name_queue["num"] ~= 0 then
        -- 清除其他任务计时
        close_delay_count = 0
        query_delay_count = 0
        check_online_count = 0

        -- 进入自己的计时
        open_delay_count = open_delay_count + 1
        if open_delay_count >= 2 then
            open_delay_count = 0
            open_group = out_queue(open_group_queue)
            open_name = out_queue(open_name_queue)
            open_mydevice()
            return
        end
    else
        open_delay_count = 0
    end

    -- 查询在线状态
    if query_queue["num"] ~= 0 then
        -- 清除其他任务计时
        check_online_count = 0
        
        query_delay_count = query_delay_count + 1
        if query_delay_count >= 2 then
            query_delay_count = 0
            query_device_isonline(out_queue(query_queue))
            -- 初始化设备计数
            if init_doing == 1 then
                init_device_count = init_device_count + 1
            end
            return
        end
    else
        query_delay_count = 0
    end

    -- 定时检查设备在线离线
    check_online_count = check_online_count + 1
    if check_online_count > 60 then
        check_online_count = 0
        check_status()
    end
end

-- http接收响应 130-0000 131-00
function on_http_response(taskid, response)
    local notify_done = 0
    local response_ip = string.match(response, "%d+")

    -- 数据验证
    if string.find(response, '-', 1) ~= nil then
        -- 查找施肥组IP
        for i = 0, record_get_count(2, 60) do
            -- 如果找到对应的IP
            local cur_read_record = record_read(2, 60, i)
            if string.find(cur_read_record, response_ip, 1) ~= nil then
                notify_done = 1
                local new_record = cur_read_record
                local id = tonumber(string.sub(string.match(cur_read_record, ";%d;"), 2, 2))
                -- 如果是离线状态
                if string.find(cur_read_record, '离', 1) ~= nil then
                    new_record = string.gsub(cur_read_record, '离', '在', 1)
                end
                -- 如果现在的开关状态和获取到的不一致
                local get_status = string.sub(response, 4 + id, 4 + id)
                local now_status = '关'
                if string.find(cur_read_record, '开', 1) ~= nil then
                    now_status = '开'
                end
                if now_status == '开' and get_status == '0' then
                    new_record = string.gsub(new_record, '开', '关', 1)
                elseif now_status == '关' and get_status == '1' then
                    new_record = string.gsub(new_record, '关', '开', 1)
                end
                -- 如果要修改的和原来不一样
                if new_record ~= cur_read_record then
                    record_modify(2, 60, i, new_record)
                end
            end
        end
        if notify_done == 1 then
            return
        end
        -- 查找打药组IP
        for i = 0, record_get_count(2, 4) do
            -- 如果找到对应的IP
            local cur_read_record = record_read(2, 4, i)
            if string.find(cur_read_record, response_ip, 1) ~= nil then
                notify_done = 1
                local new_record = cur_read_record
                local id = tonumber(string.sub(string.match(cur_read_record, ";%d;"), 2, 2))
                -- 如果是离线状态
                if string.find(cur_read_record, '离', 1) ~= nil then
                    new_record = string.gsub(cur_read_record, '离', '在', 1)
                end
                -- 如果现在的开关状态和获取到的不一致
                local get_status = string.sub(response, 4 + id, 4 + id)
                local now_status = '关'
                if string.find(cur_read_record, '开', 1) ~= nil then
                    now_status = '开'
                end
                if now_status == '开' and get_status == '0' then
                    new_record = string.gsub(new_record, '开', '关', 1)
                elseif now_status == '关' and get_status == '1' then
                    new_record = string.gsub(new_record, '关', '开', 1)
                end
                -- 如果要修改的和原来不一样
                if new_record ~= cur_read_record then
                    record_modify(2, 4, i, new_record)
                end
            end
        end
        if notify_done == 1 then
            return
        end
        -- 查找水泵组IP
        for i = 0, record_get_count(2, 7) do
            -- 如果找到对应的IP
            local cur_read_record = record_read(2, 7, i)
            if string.find(cur_read_record, response_ip, 1) ~= nil then
                local new_record = cur_read_record
                local id = tonumber(string.sub(string.match(cur_read_record, ";%d;"), 2, 2))
                -- 如果是离线状态
                if string.find(cur_read_record, '离', 1) ~= nil then
                    new_record = string.gsub(cur_read_record, '离', '在', 1)
                end
                -- 如果现在的开关状态和获取到的不一致
                local get_status = string.sub(response, 4 + id, 4 + id)
                local now_status = '关'
                if string.find(cur_read_record, '开', 1) ~= nil then
                    now_status = '开'
                end
                if now_status == '开' and get_status == '0' then
                    new_record = string.gsub(new_record, '开', '关', 1)
                elseif now_status == '关' and get_status == '1' then
                    new_record = string.gsub(new_record, '关', '开', 1)
                end
                -- 如果要修改的和原来不一样
                if new_record ~= cur_read_record then
                    record_modify(2, 7, i, new_record)
                    local name
                    if string.find(new_record,'水泵施肥',1)~=nil then
                        name = string.match(new_record,'水泵施肥%d+')
                        if record_get_count(0,23)~=0 then
                            for i = 0, record_get_count(0, 23) - 1 do
                                local read_record = record_read(0,23,i)
                                if string.find(read_record,name,1)~=nil then
                                    if string.find(new_record,'开',1)~=nil then
                                        local notify_record = name..';开;'
                                        record_modify(0,23,i,notify_record)
                                    end
                                    if string.find(new_record,'关',1)~=nil then
                                        local notify_record = name..';关;'
                                        record_modify(0,23,i,notify_record)
                                    end
                                end
                            end
                        end
                    end
                    if string.find(new_record,'水泵打药',1)~=nil then
                        name = string.match(new_record,'水泵打药%d+')
                        if record_get_count(0,36)~=0 then
                            for i = 0, record_get_count(0, 36) - 1 do
                                local read_record = record_read(0,36,i)
                                if string.find(read_record,name,1)~=nil then
                                    if string.find(new_record,'开',1)~=nil then
                                        local notify_record = name..';开;'
                                        record_modify(0,36,i,notify_record)
                                    end
                                    if string.find(new_record,'关',1)~=nil then
                                        local notify_record = name..';关;'
                                        record_modify(0,36,i,notify_record)
                                    end
                                end
                            end
                        end
                    end
                end
            end
        end
    end
end

-- 控件点击回调
function on_control_notify(screen, control, value)
    -- 界面0：“施肥开始”按钮回调
    if screen == 0 and control == 1 and value == 1 then
        if shifei_time ~= 0 or shifei_time_sec ~=0 and shifei_isWorking == 0 then
			--检查设备是否都在线
			-- if record_get_count(0, 60) > 0 then
			-- 	for i = 0, record_get_count(0, 60) - 1 do
			-- 		local cur_read_record = record_read(0,60,i)
			-- 		local name = string.match(cur_read_record, "施肥%d+")
			-- 		if record_get_count(2, 60) > 0 then
			-- 			for j = 0, record_get_count(2, 60) - 1 do
			-- 				local cur_read_record1 = record_read(2,60,j)
			-- 				if string.find(cur_read_record1,name,1) ~= nil then
			-- 					if string.find(cur_read_record1,"离",1) ~= nil then
			-- 						print("device is not online")
			-- 						change_screen(7)
			-- 						return
			-- 					end
			-- 				end
			-- 			end
			-- 		end
			-- 	end
			-- end

            if record_get_count(0, 60) > 0 then
                -- 关闭定时器
                stop_timer(0)
                -- 工作状态复位为未开始
                for i = 0, record_get_count(0, 60) - 1 do
                    local cur_read_record = record_read(0, 60, i)
                    local name = string.match(cur_read_record, "施肥%d+")
                    local new_record = name .. ";未开始;"
                    record_modify(0, 60, i, new_record)
                end
                -- 修改正在工作状态：工作中
                local cur_read_record = record_read(0, 60, 0);
                local new_record = string.gsub(cur_read_record, "未开始", "工作中", 1);
                record_modify(0, 60, 0, new_record)
                -- 打开定时器，打开继电器
                start_timer(0, 1000, 0, 0)
                local name = string.match(cur_read_record, "施肥%d+")
                open_device("施肥组", name)
                print("正在工作:" .. name)
                shifei_timer_count = 0
                -- 将控件设置为不可触摸
                set_enable(0, 1, 0)
                shifei_isWorking = 1
            end
        else
            print("请先设置工作时间间隔")
            change_child_screen(5)
        end
    end
    -- 界面0：“打药开始”按钮回调
    if screen == 0 and control == 3 and value == 1 then
        if dayao_time ~= 0 and dayao_isWorking == 0 then
			--检查设备是否都在线
			if record_get_count(0, 19) > 0 then
				for i = 0, record_get_count(0, 19) - 1 do
					local cur_read_record = record_read(0,19,i)
					local name = string.match(cur_read_record, "打药%d+")
					if record_get_count(2, 4) > 0 then
						for j = 0, record_get_count(2, 4) - 1 do
							local cur_read_record1 = record_read(2,4,j)
							if string.find(cur_read_record1,name,1) ~= nil then
								if string.find(cur_read_record1,"离",1) ~= nil then
									print("device is not online")
									change_screen(7)
									return
								end
							end
						end
					end
				end
			end

            if record_get_count(0, 19) > 0 then
                -- 关闭定时器
                stop_timer(1)
                -- 工作状态复位为未开始
                for i = 0, record_get_count(0, 19) - 1 do
                    local cur_read_record = record_read(0, 19, i)
                    local name = string.match(cur_read_record, "打药%d+")
                    local new_record = name .. ";未开始;"
                    record_modify(0, 19, i, new_record)
                end
                -- 修改正在工作状态：工作中
                local cur_read_record = record_read(0, 19, 0);
                local new_record = string.gsub(cur_read_record, "未开始", "工作中", 1);
                record_modify(0, 19, 0, new_record)
                -- 打开定时器，打开继电器
                start_timer(1, 1000, 0, 0)
                local name = string.match(cur_read_record, "打药%d+")
                open_device("打药组", name)
                print("正在工作:" .. name)
                dayao_timer_count = 0
                -- 将控件设置为不可触摸
                set_enable(0, 3, 0)
                dayao_isWorking = 1
            end
        else
            print("请先设置工作时间间隔")
            change_child_screen(5)
        end
    end
    -- 界面0：“水泵施肥开始”按钮回调
    if screen == 0 and control == 24 and value == 1 then
        if shuibeng_shifei_time ~= 0 and shuibeng_shifei_isWorking == 0 then
			--检查设备是否都在线
            if record_get_count(2, 7) > 0 then
                for i = 0, record_get_count(2, 7) - 1 do
                    local cur_read_record = record_read(2,7,i)
                    if string.find(cur_read_record,'离',1)~=nil then
                        print("device is not online")
                        change_screen(7)
                        return
                    end
                end
            end
            --启动全部设备
            if record_get_count(0, 23) > 0 then
                -- 关闭定时器
                stop_timer(2)
                -- 打开继电器，工作状态设置为开
                for i = 0, record_get_count(0, 23) - 1 do
                    local cur_read_record = record_read(0, 23, i)
                    local name = string.match(cur_read_record, "水泵施肥%d+")
                    local new_record = name .. ";开;"
                    record_modify(0, 23, i, new_record)
                    open_device("水泵组", name)
                end
                -- 打开定时器，打开继电器
                shuibeng_shifei_timer_count = 0
                start_timer(2, 1000, 0, 0)
                -- 将控件设置为不可触摸
                set_enable(0, 24, 0)
                shuibeng_shifei_isWorking = 1
            end
        else
            print("请先设置工作时间间隔")
            change_child_screen(5)
        end
    end
    -- 界面0：“水泵打药开始”按钮回调
    if screen == 0 and control == 33 and value == 1 then
        if shuibeng_dayao_time ~= 0 and shuibeng_dayao_isWorking == 0 then
            --检查设备是否都在线
            if record_get_count(2, 7) > 0 then
                for i = 0, record_get_count(2, 7) - 1 do
                    local cur_read_record = record_read(2,7,i)
                    if string.find(cur_read_record,'离',1)~=nil then
                        print("device is not online")
                        change_screen(7)
                        return
                    end
                end
            end
            --启动全部设备
            if record_get_count(0, 36) > 0 then
                -- 关闭定时器
                stop_timer(3)
                -- 打开继电器，工作状态设置为开
                for i = 0, record_get_count(0, 36) - 1 do
                    local cur_read_record = record_read(0, 36, i)
                    local name = string.match(cur_read_record, "水泵打药%d+")
                    local new_record = name .. ";开;"
                    record_modify(0, 36, i, new_record)
                    open_device("水泵组", name)
                end
                -- 打开定时器，打开继电器
                shuibeng_dayao_timer_count = 0
                shuibeng_dayao_isWorking = 1
                start_timer(3, 1000, 0, 0)
                -- 将控件设置为不可触摸
                set_enable(0, 33, 0)
            end
        else
            print("请先设置工作时间间隔")
            change_child_screen(5)
        end
    end

    -- 界面0：施肥“暂停”按钮回调
    if screen == 0 and control == 9 and value == 1 then
        if get_text(0, 44) == "暂停" and shifei_isWorking == 1 then
            shifei_isWorking = 0
            set_text(0, 44, "继续")
            stop_timer(0);
            -- 查找当前工作的设备，序号
            local record
            local locate
            for i = 0, record_get_count(0, 60) - 1 do
                record = record_read(0, 60, i)
                if string.find(record, "工作中", 1) ~= nil then
                    locate = i
                    break
                end
            end
            -- 修改正在工作状态：暂停中，关闭继电器
            local new_record = string.gsub(record, "工作中", "暂停中", 1);
            record_modify(0, 60, locate, new_record)
            local name = string.match(record, "施肥%d+")
            close_device("施肥组", name)
            print("暂停中:" .. name)

            -- 客户要求：把水泵也一起暂停了
            set_text(0, 54, "继续")
            set_enable(0,24,1)
            set_enable(0,33,1)
            stop_timer(2);
            stop_timer(3);
            if shuibeng_shifei_isWorking == 1 then
                -- 关闭全部继电器，修改正在工作状态：关
                for i = 0, record_get_count(0, 23) - 1 do
                    local cur_read_record = record_read(0,23,i)
                    local name = string.match(cur_read_record, "水泵施肥%d+")
                    local new_record = name .. ";关;"
                    record_modify(0, 23, i, new_record)
                    close_device("水泵组", name)
                end
            end
            if shuibeng_dayao_isWorking == 1 then
                -- 关闭全部继电器，修改正在工作状态：关
                for i = 0, record_get_count(0, 36) - 1 do
                    local cur_read_record = record_read(0,36,i)
                    local name = string.match(cur_read_record, "水泵打药%d+")
                    local new_record = name .. ";关;"
                    record_modify(0, 36, i, new_record)
                    close_device("水泵组", name)
                end
            end
            shuibeng_shifei_isWorking = 0
            shuibeng_dayao_isWorking = 0
        elseif get_text(0, 44) == "继续" and shifei_isWorking == 0 then
            shifei_isWorking = 1
            set_text(0, 44, "暂停")
            start_timer(0, 1000, 0, 0)
            -- 查找当前暂停中设备，序号
            local record
            local locate
            for i = 0, record_get_count(0, 60) - 1 do
                record = record_read(0, 60, i)
                if string.find(record, "暂停中", 1) ~= nil then
                    locate = i
                    break
                end
            end
            -- 修改正在工作状态：工作中
            local new_record = string.gsub(record, "暂停中", "工作中", 1);
            record_modify(0, 60, locate, new_record)
            -- 串口发送开始
            local name = string.match(record, "施肥%d+")
            open_device("施肥组", name)
            print("工作中:" .. name)
        end
    end
    -- 界面0：打药“暂停”按钮回调
    if screen == 0 and control == 8 and value == 1 then
        if get_text(0, 49) == "暂停" and dayao_isWorking == 1 then
            dayao_isWorking = 0
            set_text(0, 49, "继续")
            stop_timer(1);
            -- 查找当前工作的设备，序号
            local record
            local locate
            for i = 0, record_get_count(0, 19) - 1 do
                record = record_read(0, 19, i)
                if string.find(record, "工作中", 1) ~= nil then
                    locate = i
                    break
                end
            end
            -- 修改正在工作状态：暂停中，关闭继电器
            local new_record = string.gsub(record, "工作中", "暂停中", 1);
            record_modify(0, 19, locate, new_record)
            local name = string.match(record, "打药%d+")
            close_device("打药组", name)
            print("暂停中:" .. name)

            -- 客户要求：把水泵也一起暂停了
            set_text(0, 54, "继续")
            set_enable(0,24,1)
            set_enable(0,33,1)
            stop_timer(2);
            stop_timer(3);
            if shuibeng_shifei_isWorking == 1 then
                -- 关闭全部继电器，修改正在工作状态：关
                for i = 0, record_get_count(0, 23) - 1 do
                    local cur_read_record = record_read(0,23,i)
                    local name = string.match(cur_read_record, "水泵施肥%d+")
                    local new_record = name .. ";关;"
                    record_modify(0, 23, i, new_record)
                    close_device("水泵组", name)
                end
            end
            if shuibeng_dayao_isWorking == 1 then
                -- 关闭全部继电器，修改正在工作状态：关
                for i = 0, record_get_count(0, 36) - 1 do
                    local cur_read_record = record_read(0,36,i)
                    local name = string.match(cur_read_record, "水泵打药%d+")
                    local new_record = name .. ";关;"
                    record_modify(0, 36, i, new_record)
                    close_device("水泵组", name)
                end
            end
            shuibeng_shifei_isWorking = 0
            shuibeng_dayao_isWorking = 0
        elseif get_text(0, 49) == "继续" and dayao_isWorking == 0 then
            dayao_isWorking = 1
            set_text(0, 49, "暂停")
            start_timer(1, 1000, 0, 0)
            -- 查找当前暂停中设备，序号
            local record
            local locate
            for i = 0, record_get_count(0, 19) - 1 do
                record = record_read(0, 19, i)
                if string.find(record, "暂停中", 1) ~= nil then
                    locate = i
                    break
                end
            end
            -- 修改正在工作状态：工作中
            local new_record = string.gsub(record, "暂停中", "工作中", 1);
            record_modify(0, 19, locate, new_record)
            -- 串口发送开始
            local name = string.match(record, "打药%d+")
            open_device("打药组", name)
            print("工作中:" .. name)
        end
    end
    -- 界面0：水泵“暂停”按钮回调
    if screen == 0 and control == 27 and value == 1 then
        if get_text(0, 54) == "暂停" and (shuibeng_shifei_isWorking + shuibeng_dayao_isWorking >= 1 ) then
            set_text(0, 54, "继续")
            set_enable(0,24,1)
            set_enable(0,33,1)
            stop_timer(2);
            stop_timer(3);
            if shuibeng_shifei_isWorking == 1 then
                -- 关闭全部继电器，修改正在工作状态：关
                for i = 0, record_get_count(0, 23) - 1 do
                    local cur_read_record = record_read(0,23,i)
                    local name = string.match(cur_read_record, "水泵施肥%d+")
                    local new_record = name .. ";关;"
                    record_modify(0, 23, i, new_record)
                    close_device("水泵组", name)
                end
            end
            if shuibeng_dayao_isWorking == 1 then
                -- 关闭全部继电器，修改正在工作状态：关
                for i = 0, record_get_count(0, 36) - 1 do
                    local cur_read_record = record_read(0,36,i)
                    local name = string.match(cur_read_record, "水泵打药%d+")
                    local new_record = name .. ";关;"
                    record_modify(0, 36, i, new_record)
                    close_device("水泵组", name)
                end
            end
            shuibeng_shifei_isWorking = 0
            shuibeng_dayao_isWorking = 0
        elseif get_text(0, 54) == "继续" and shuibeng_shifei_isWorking == 0 and shuibeng_dayao_isWorking == 0 then
            shuibeng_shifei_isWorking = 1
            shuibeng_dayao_isWorking = 1
            set_text(0, 54, "暂停")
            set_enable(0,24,0)
            set_enable(0,33,0)
            start_timer(2, 1000, 0, 0)
            start_timer(3, 1000, 0, 0)
            -- 打开全部继电器，修改正在工作状态：开
            for i = 0, record_get_count(0, 23) - 1 do
                local cur_read_record = record_read(0,23,i)
                local name = string.match(cur_read_record, "水泵施肥%d+")
                local new_record = name .. ";开;"
                record_modify(0, 23, i, new_record)
                open_device("水泵组", name)
            end
            for i = 0, record_get_count(0, 36) - 1 do
                local cur_read_record = record_read(0,36,i)
                local name = string.match(cur_read_record, "水泵打药%d+")
                local new_record = name .. ";开;"
                record_modify(0, 36, i, new_record)
                open_device("水泵组", name)
            end
        end
    end

    -- 界面0：施肥“停止”按钮回调
    if screen == 0 and control == 11 and value == 1 then
        set_text(0, 44, "暂停")
        set_enable(0, 1, 1)
        stop_timer(0)
        shifei_isWorking = 0
        -- 如果是工作状态，查找当前工作中设备，发送关闭继电器指令
        local record
        local locate
        for i = 0, record_get_count(0, 60) - 1 do
            record = record_read(0, 60, i)
            if string.find(record, "工作中", 1) ~= nil then
                print("施肥停止")
                local name = string.match(record, "施肥%d+")
                close_device("施肥组", name)
                break
            end
        end
        -- 工作状态复位为未开始
        for i = 0, record_get_count(0, 60) - 1 do
            local cur_read_record = record_read(0, 60, i)
            local name = string.match(cur_read_record, "施肥%d+")
            local new_record = name .. ";未开始;"
            record_modify(0, 60, i, new_record)
        end

        -- 客户要求：把水泵也停止了
        set_text(0, 54, "暂停")
        set_enable(0,24,1)
        set_enable(0,33,1)
        stop_timer(2);
        stop_timer(3);
        if shuibeng_shifei_isWorking == 1 then
            -- 关闭全部继电器，修改正在工作状态：关
            for i = 0, record_get_count(0, 23) - 1 do
                local cur_read_record = record_read(0,23,i)
                local name = string.match(cur_read_record, "水泵施肥%d+")
                local new_record = name .. ";关;"
                record_modify(0, 23, i, new_record)
                close_device("水泵组", name)
            end
        end
        if shuibeng_dayao_isWorking == 1 then
            -- 关闭全部继电器，修改正在工作状态：关
            for i = 0, record_get_count(0, 36) - 1 do
                local cur_read_record = record_read(0,36,i)
                local name = string.match(cur_read_record, "水泵打药%d+")
                local new_record = name .. ";关;"
                record_modify(0, 36, i, new_record)
                close_device("水泵组", name)
            end
        end
        shuibeng_shifei_isWorking = 0
        shuibeng_dayao_isWorking = 0
        shuibeng_shifei_timer_count = 0
        shuibeng_dayao_timer_count = 0
    end
    -- 界面0：打药“停止”按钮回调
    if screen == 0 and control == 14 and value == 1 then
        set_text(0, 49, "暂停")
        set_enable(0, 3, 1)
        stop_timer(1)
        dayao_isWorking = 0
        -- 如果是工作状态，查找当前工作中设备，发送关闭继电器指令
        local record
        local locate
        for i = 0, record_get_count(0, 19) - 1 do
            record = record_read(0, 19, i)
            if string.find(record, "工作中", 1) ~= nil then
                print("打药停止")
                local name = string.match(record, "打药%d+")
                close_device("打药组", name)
                break
            end
        end
        -- 工作状态复位为未开始
        for i = 0, record_get_count(0, 19) - 1 do
            local cur_read_record = record_read(0, 19, i)
            local name = string.match(cur_read_record, "打药%d+")
            local new_record = name .. ";未开始;"
            record_modify(0, 19, i, new_record)
        end

        -- 客户要求：把水泵也停止了
        set_text(0, 54, "暂停")
        set_enable(0,24,1)
        set_enable(0,33,1)
        stop_timer(2);
        stop_timer(3);
        if shuibeng_shifei_isWorking == 1 then
            -- 关闭全部继电器，修改正在工作状态：关
            for i = 0, record_get_count(0, 23) - 1 do
                local cur_read_record = record_read(0,23,i)
                local name = string.match(cur_read_record, "水泵施肥%d+")
                local new_record = name .. ";关;"
                record_modify(0, 23, i, new_record)
                close_device("水泵组", name)
            end
        end
        if shuibeng_dayao_isWorking == 1 then
            -- 关闭全部继电器，修改正在工作状态：关
            for i = 0, record_get_count(0, 36) - 1 do
                local cur_read_record = record_read(0,36,i)
                local name = string.match(cur_read_record, "水泵打药%d+")
                local new_record = name .. ";关;"
                record_modify(0, 36, i, new_record)
                close_device("水泵组", name)
            end
        end
        shuibeng_shifei_isWorking = 0
        shuibeng_dayao_isWorking = 0
        shuibeng_shifei_timer_count = 0
        shuibeng_dayao_timer_count = 0
    end
    -- 界面0：水泵“停止”按钮回调
    if screen == 0 and control == 29 and value == 1 then
        set_text(0, 54, "暂停")
        set_enable(0,24,1)
        set_enable(0,33,1)
        stop_timer(2);
        stop_timer(3);
        if shuibeng_shifei_isWorking == 1 then
            -- 关闭全部继电器，修改正在工作状态：关
            for i = 0, record_get_count(0, 23) - 1 do
                local cur_read_record = record_read(0,23,i)
                local name = string.match(cur_read_record, "水泵施肥%d+")
                local new_record = name .. ";关;"
                record_modify(0, 23, i, new_record)
                close_device("水泵组", name)
            end
        end
        if shuibeng_dayao_isWorking == 1 then
            -- 关闭全部继电器，修改正在工作状态：关
            for i = 0, record_get_count(0, 36) - 1 do
                local cur_read_record = record_read(0,36,i)
                local name = string.match(cur_read_record, "水泵打药%d+")
                local new_record = name .. ";关;"
                record_modify(0, 36, i, new_record)
                close_device("水泵组", name)
            end
        end
        shuibeng_shifei_isWorking = 0
        shuibeng_dayao_isWorking = 0
        shuibeng_shifei_timer_count = 0
        shuibeng_dayao_timer_count = 0
    end

    -- 界面0：施肥组选择继电器【记录】点击
    if screen == 0 and control == 39 then
        shifei_select = value
    end
    -- 界面0：打药组选择继电器【记录】点击
    if screen == 0 and control == 45 then
        dayao_select = value
    end

    -- 界面0：施肥组【添加】继电器按钮回调		
    if screen == 0 and control == 53 and value == 1 then
        -- 隐藏选择项【记录】
        set_visiable(0, 39, 0)
        set_visiable(0, 40, 1)

        -- 获取用户输入数据
        local m_name = record_read(0, 39, shifei_select);
        -- 若空，直接返回
        if m_name == "" then
            return
        end
        -- 添加到任务列表中
        local cur_add_record = m_name .. "未开始;";
        record_add(0, 60, cur_add_record);
    end

    -- 界面0：施肥组【删除】继电器按钮回调		
    if screen == 0 and control == 61 and value == 1 then
        -- 隐藏选择项【记录】
        set_visiable(0, 39, 0)
        set_visiable(0, 40, 1)

        -- 获取用户输入数据
        local m_name = record_read(0, 39, shifei_select)
        -- 若空，直接返回
        if m_name == "" then
            return
        end
        -- 倒序查找记录
        local recordCnt = record_get_count(0, 60)
        if recordCnt == 0 then
            return
        end
        local i = recordCnt - 1
        for i = recordCnt - 1, 0, -1 do
            local cur_read_record = record_read(0, 60, i)
            if string.find(cur_read_record, m_name, 1) ~= nil then
                record_delete(0, 60, i)
                return
            end
        end
    end
    -- 界面0：打药组【添加】继电器按钮回调		
    if screen == 0 and control == 17 and value == 1 then
        -- 隐藏选择项【记录】
        set_visiable(0, 45, 0)
        set_visiable(0, 48, 1)

        -- 获取用户输入数据
        local m_name = record_read(0, 45, dayao_select);
        -- 若空，直接返回
        if m_name == "" then
            return
        end
        -- 添加到任务列表中
        local cur_add_record = m_name .. "未开始;";
        record_add(0, 19, cur_add_record);
    end
    -- 界面0：打药组【删除】继电器按钮回调		
    if screen == 0 and control == 20 and value == 1 then
        -- 隐藏选择项【记录】
        set_visiable(0, 45, 0)
        set_visiable(0, 48, 1)

        -- 获取用户输入数据
        local m_name = record_read(0, 45, dayao_select)
        -- 若空，直接返回
        if m_name == "" then
            return
        end
        -- 倒序查找记录
        local recordCnt = record_get_count(0, 19)
        if recordCnt == 0 then
            return
        end
        local i = recordCnt - 1
        for i = recordCnt - 1, 0, -1 do
            local cur_read_record = record_read(0, 19, i)
            if string.find(cur_read_record, m_name, 1) ~= nil then
                record_delete(0, 19, i)
                return;
            end
        end
    end
    -- 画面0：添加施肥继电器选择【按钮】
    if screen == 0 and control == 40 and value == 1 then
        record_clear(0, 39)
        set_visiable(0, 39, 1)
        set_visiable(0, 40, 0)
        if record_get_count(2, 60) ~= 0 then
            for i = 0, record_get_count(2, 60) - 1 do
                local cur_read_record = record_read(2, 60, i)
                local cur_add_record = string.match(cur_read_record, "施肥%d+;")
                record_add(0, 39, cur_add_record)
            end
        end
        return
    end
    -- 画面0：添加打药继电器选择【按钮】
    if screen == 0 and control == 48 and value == 1 then
        record_clear(0, 45)
        set_visiable(0, 45, 1)
        set_visiable(0, 48, 0)
        if record_get_count(2, 4) ~= 0 then
            for i = 0, record_get_count(2, 4) - 1 do
                local cur_read_record = record_read(2, 4, i)
                local cur_add_record = string.match(cur_read_record, "打药%d+;")
                record_add(0, 45, cur_add_record)
            end
        end
        return
    end

    -- 界面0：水泵施肥继电器“开关”按钮回调
    if screen == 0 and control == 51 and value == 1 then
        local record_read1 = record_read(0,23,0)
        if string.find(record_read1,'水泵施肥',1)~=nil then
            local name = string.match(record_read1,'水泵施肥%d+')
            if record_get_count(2,7) ~= 0 then
                for i = 0, record_get_count(2, 7) - 1 do
                    local cur_read_record = record_read(2,7,i)
                    if string.find(cur_read_record,name,1) ~= nil then
                        if string.find(cur_read_record,'离',1)~=nil then
                            change_screen(7)
                            return
                        end
                        if string.find(cur_read_record,'关',1) ~= nil then
                            open_device("水泵组", name)
                            return
                        end
                        if string.find(cur_read_record,'开',1) ~= nil then
                            close_device("水泵组", name)
                            return
                        end
                    end
                end
            end
        end
    end
    if screen == 0 and control == 56 and value == 1 then
        local record_read1 = record_read(0,23,1)
        if string.find(record_read1,'水泵施肥',1)~=nil then
            local name = string.match(record_read1,'水泵施肥%d+')
            if record_get_count(2,7) ~= 0 then
                for i = 0, record_get_count(2, 7) - 1 do
                    local cur_read_record = record_read(2,7,i)
                    if string.find(cur_read_record,name,1) ~= nil then
                        if string.find(cur_read_record,'离',1)~=nil then
                            change_screen(7)
                            return
                        end
                        if string.find(cur_read_record,'关',1) ~= nil then
                            open_device("水泵组", name)
                            return
                        end
                        if string.find(cur_read_record,'开',1) ~= nil then
                            close_device("水泵组", name)
                            return
                        end
                    end
                end
            end
        end
    end
    if screen == 0 and control == 59 and value == 1 then
        local record_read1 = record_read(0,23,2)
        if string.find(record_read1,'水泵施肥',1)~=nil then
            local name = string.match(record_read1,'水泵施肥%d+')
            if record_get_count(2,7) ~= 0 then
                for i = 0, record_get_count(2, 7) - 1 do
                    local cur_read_record = record_read(2,7,i)
                    if string.find(cur_read_record,name,1) ~= nil then
                        if string.find(cur_read_record,'离',1)~=nil then
                            change_screen(7)
                            return
                        end
                        if string.find(cur_read_record,'关',1) ~= nil then
                            open_device("水泵组", name)
                            return
                        end
                        if string.find(cur_read_record,'开',1) ~= nil then
                            close_device("水泵组", name)
                            return
                        end
                    end
                end
            end
        end
    end
    if screen == 0 and control == 65 and value == 1 then
        local record_read1 = record_read(0,23,3)
        if string.find(record_read1,'水泵施肥',1)~=nil then
            local name = string.match(record_read1,'水泵施肥%d+')
            if record_get_count(2,7) ~= 0 then
                for i = 0, record_get_count(2, 7) - 1 do
                    local cur_read_record = record_read(2,7,i)
                    if string.find(cur_read_record,name,1) ~= nil then
                        if string.find(cur_read_record,'离',1)~=nil then
                            change_screen(7)
                            return
                        end
                        if string.find(cur_read_record,'关',1) ~= nil then
                            open_device("水泵组", name)
                            return
                        end
                        if string.find(cur_read_record,'开',1) ~= nil then
                            close_device("水泵组", name)
                            return
                        end
                    end
                end
            end
        end
    end
    -- 界面0：水泵打药继电器“开关”按钮回调
    if screen == 0 and control == 68 and value == 1 then
        local record_read1 = record_read(0,36,0)
        if string.find(record_read1,'水泵打药',1)~=nil then
            local name = string.match(record_read1,'水泵打药%d+')
            if record_get_count(2,7) ~= 0 then
                for i = 0, record_get_count(2, 7) - 1 do
                    local cur_read_record = record_read(2,7,i)
                    if string.find(cur_read_record,name,1) ~= nil then
                        if string.find(cur_read_record,'离',1)~=nil then
                            change_screen(7)
                            return
                        end
                        if string.find(cur_read_record,'关',1) ~= nil then
                            open_device("水泵组", name)
                            return
                        end
                        if string.find(cur_read_record,'开',1) ~= nil then
                            close_device("水泵组", name)
                            return
                        end
                    end
                end
            end
        end
    end
    if screen == 0 and control == 71 and value == 1 then
        local record_read1 = record_read(0,36,1)
        if string.find(record_read1,'水泵打药',1)~=nil then
            local name = string.match(record_read1,'水泵打药%d+')
            if record_get_count(2,7) ~= 0 then
                for i = 0, record_get_count(2, 7) - 1 do
                    local cur_read_record = record_read(2,7,i)
                    if string.find(cur_read_record,name,1) ~= nil then
                        if string.find(cur_read_record,'离',1)~=nil then
                            change_screen(7)
                            return
                        end
                        if string.find(cur_read_record,'关',1) ~= nil then
                            open_device("水泵组", name)
                            return
                        end
                        if string.find(cur_read_record,'开',1) ~= nil then
                            close_device("水泵组", name)
                            return
                        end
                    end
                end
            end
        end
    end
    if screen == 0 and control == 74 and value == 1 then
        local record_read1 = record_read(0,36,2)
        if string.find(record_read1,'水泵打药',1)~=nil then
            local name = string.match(record_read1,'水泵打药%d+')
            if record_get_count(2,7) ~= 0 then
                for i = 0, record_get_count(2, 7) - 1 do
                    local cur_read_record = record_read(2,7,i)
                    if string.find(cur_read_record,name,1) ~= nil then
                        if string.find(cur_read_record,'离',1)~=nil then
                            change_screen(7)
                            return
                        end
                        if string.find(cur_read_record,'关',1) ~= nil then
                            open_device("水泵组", name)
                            return
                        end
                        if string.find(cur_read_record,'开',1) ~= nil then
                            close_device("水泵组", name)
                            return
                        end
                    end
                end
            end
        end
    end
    if screen == 0 and control == 77 and value == 1 then
        local record_read1 = record_read(0,36,3)
        if string.find(record_read1,'水泵打药',1)~=nil then
            local name = string.match(record_read1,'水泵打药%d+')
            if record_get_count(2,7) ~= 0 then
                for i = 0, record_get_count(2, 7) - 1 do
                    local cur_read_record = record_read(2,7,i)
                    if string.find(cur_read_record,name,1) ~= nil then
                        if string.find(cur_read_record,'离',1)~=nil then
                            change_screen(7)
                            return
                        end
                        if string.find(cur_read_record,'关',1) ~= nil then
                            open_device("水泵组", name)
                            return
                        end
                        if string.find(cur_read_record,'开',1) ~= nil then
                            close_device("水泵组", name)
                            return
                        end
                    end
                end
            end
        end
    end

    --*********************************界面1********************************* --

    -- 界面1：施肥继电器工作时间间隔“确定”按钮回调		
    if screen == 1 and control == 2 and value == 1 then
        -- 更新工作时间
        shifei_time = tonumber(get_text(1, 1))
        write_flash_string(30000, get_text(1, 1))
        shifei_time_sec = tonumber(get_text(1, 36))
        write_flash_string(30001, get_text(1, 1))
        flush_flash()
    end

    -- 界面1：打药继电器工作时间间隔“确定”按钮回调		
    if screen == 1 and control == 8 and value == 1 then
        -- 更新工作时间
        dayao_time = tonumber(get_text(1, 7))
        write_flash_string(30010, get_text(1, 7))
        dayao_time_sec = tonumber(get_text(1, 37))
        write_flash_string(30011, get_text(1, 37))
        flush_flash()
    end

    -- 界面1：水泵施肥继电器工作时间间隔“确定”按钮回调                         		
    if screen == 1 and control == 13 and value == 1 then
        -- 更新工作时间
        shuibeng_shifei_time = tonumber(get_text(1, 12))
        write_flash_string(30020, get_text(1, 12))
        shuibeng_shifei_time_sec = tonumber(get_text(1, 38))
        write_flash_string(30021, get_text(1, 38))
        flush_flash()
    end

    -- 界面1：水泵打药继电器工作时间间隔“确定”按钮回调		
    if screen == 1 and control == 26 and value == 1 then
        -- 更新工作时间
        shuibeng_dayao_time = tonumber(get_text(1, 25))
        write_flash_string(30030, get_text(1, 25))
        shuibeng_dayao_time_sec = tonumber(get_text(1, 39))
        write_flash_string(30031, get_text(1, 39))
        flush_flash()
    end

    --*********************************界面3********************************* --

    -- 界面3：添加继电器“确定”按钮回调
    if screen == 3 and control == 7 and value == 1 then
        -- 读取组别
        local add_group = get_text(3, 4)
        -- 读取数量
        local add_num = get_text(3, 14)
        -- 读取IP
        local ip = get_text(3, 1)

        if add_group == "施肥组" then
            -- 数据核查，是否重复
            local shifei_max_num = 0
            for i = 0, record_get_count(2, 60) do
                -- 如果找到相同的IP
                local cur_read_record = record_read(2, 60, i)
                if string.find(cur_read_record, ip, 1) ~= nil then
                    print("不可重复添加")
                    return
                end
                -- 提取继电器设备名的编号数，保存最大的编号
                if cur_read_record ~= "" then
                    local num = string.match(cur_read_record, "施肥%d+;")
                    num = string.match(num, "%d+")
                    num = tonumber(num)
                    if num > shifei_max_num then
                        shifei_max_num = num
                    end
                end
            end
            -- 添加到继电器设备列表
            for i = 1, add_num do
                local cur_add_record = ip .. ";施肥" .. shifei_max_num + i .. ";" .. tostring(i) .. ";离;关;"
                record_add(2, 60, cur_add_record)
            end
            print("施肥继电器添加成功")
            in_queue(query_queue, tonumber(ip))
        end
        if add_group == "打药组" then
            -- 数据核查，是否重复
            local dayao_max_num = 0
            for i = 0, record_get_count(2, 4) do
                -- 如果找到相同的IP
                local cur_read_record = record_read(2, 4, i)
                if string.find(cur_read_record, ip, 1) ~= nil then
                    print("不可重复添加")
                    return
                end
                -- 提取继电器设备名的编号数，保存最大的编号
                if cur_read_record ~= "" then
                    local num = string.match(cur_read_record, "打药%d+;")
                    num = string.match(num, "%d+")
                    num = tonumber(num)
                    if num > dayao_max_num then
                        dayao_max_num = num
                    end
                end
            end
            -- 添加到继电器设备列表
            for i = 1, add_num do
                local cur_add_record = ip .. ";打药" .. dayao_max_num + i .. ";" .. tostring(i) .. ";离;关;"
                record_add(2, 4, cur_add_record)
            end
            print("打药继电器添加成功")
            in_queue(query_queue, tonumber(ip))
        end
        if add_group == "水泵施肥组" then
            -- 数据核查，是否满了，最多4个
            if record_get_count(0,23) + add_num > 4 then
                print("超过添加数量")
                return
            end
            -- 数据核查，是否IP重复
            local shuibeng_shifei_max_num = 0
            for i = 0, record_get_count(2, 7) do
                -- 如果找到相同的IP
                local cur_read_record = record_read(2, 7, i)
                if string.find(cur_read_record, ip, 1) ~= nil then
                    print("不可重复添加")
                    return
                end
                -- 提取继电器设备名的编号数，保存最大的编号
                if string.find(cur_read_record,'水泵施肥',1) ~= nil then
                    local num = string.match(cur_read_record, "水泵施肥%d+;")
                    num = string.match(num, "%d+")
                    num = tonumber(num)
                    if num > shuibeng_shifei_max_num then
                        shuibeng_shifei_max_num = num
                    end
                end
            end
            -- 添加到继电器设备列表
            for i = 1, add_num do
                local cur_add_record = ip .. ";水泵施肥" .. shuibeng_shifei_max_num + i .. ";" .. tostring(i) .. ";离;关;"
                record_add(2, 7, cur_add_record)
                local cur_add_record1 = "水泵施肥" .. shuibeng_shifei_max_num + i .. ";关;"
                record_add(0, 23, cur_add_record1)
            end
            print("水泵施肥继电器添加成功")
            in_queue(query_queue, tonumber(ip))
        end
        if add_group == "水泵打药组" then
            -- 数据核查，是否满了，最多4个
            if record_get_count(0,36) + add_num > 4 then
                print("超过添加数量")
                return
            end
            -- 数据核查，是否IP重复
            local shuibeng_dayao_max_num = 0
            for i = 0, record_get_count(2, 7) do
                -- 如果找到相同的IP
                local cur_read_record = record_read(2, 7, i)
                if string.find(cur_read_record, ip, 1) ~= nil then
                    print("不可重复添加")
                    return
                end
                -- 提取继电器设备名的编号数，保存最大的编号
                if string.find(cur_read_record,'水泵打药',1) ~= nil then
                    local num = string.match(cur_read_record, "水泵打药%d+;")
                    num = string.match(num, "%d+")
                    num = tonumber(num)
                    if num > shuibeng_dayao_max_num then
                        shuibeng_dayao_max_num = num
                    end
                end
            end
            -- 添加到继电器设备列表
            for i = 1, add_num do
                local cur_add_record = ip .. ";水泵打药" .. shuibeng_dayao_max_num + i .. ";" .. tostring(i) .. ";离;关;"
                record_add(2, 7, cur_add_record)
                local cur_add_record1 = "水泵打药" .. shuibeng_dayao_max_num + i .. ";关;"
                record_add(0, 36, cur_add_record1)
            end
            print("水泵打药继电器添加成功")
            in_queue(query_queue, tonumber(ip))
        end

    end

    -- 界面3：删除继电器菜单点击回调
    if screen == 3 and control == 49 then
        -- 清空记录
        record_clear(3, 11)

        -- 施肥组
        if value == 0 then
            if record_get_count(2, 60) ~= 0 then
                for i = 0, record_get_count(2, 60) - 1 do
                    local cur_read_record = record_read(2, 60, i)
                    local cur_add_record = string.match(cur_read_record, "施肥%d+")
                    record_add(3, 11, cur_add_record)
                end
            end
            return
        end
        -- 打药组
        if value == 1 then
            if record_get_count(2, 4) ~= 0 then
                for i = 0, record_get_count(2, 4) - 1 do
                    local cur_read_record = record_read(2, 4, i)
                    local cur_add_record = string.match(cur_read_record, "打药%d+")
                    record_add(3, 11, cur_add_record)
                end
            end
            return
        end
        -- 水泵组
        if value == 2 then
            if record_get_count(2, 7) ~= 0 then
                for i = 0, record_get_count(2, 7) - 1 do
                    local cur_read_record = record_read(2, 7, i)
                    if string.find(cur_read_record,'水泵施肥',1)~=nil then
                        local cur_add_record = string.match(cur_read_record, "水泵施肥%d+")
                        record_add(3, 11, cur_add_record)               
                    end
                    if string.find(cur_read_record,'水泵打药',1)~=nil then
                        local cur_add_record = string.match(cur_read_record, "水泵打药%d+")
                        record_add(3, 11, cur_add_record)               
                    end
                end
            end
            return
        end
    end

    -- 界面3：删除继电器记录点击回调
    if screen == 3 and control == 11 then
        delete_record_value = value
    end

    -- 界面3：【删除】继电器按钮回调
    if screen == 3 and control == 46 and value == 1 then
        -- 读取组别
        local delete_group = get_text(3, 42)
        -- 读取选中设备名
        local delete_device = record_read(3, 11, delete_record_value)

        -- 删除选中项
        if delete_group == "施肥组" then
            if record_get_count(2, 60) > 0 then
                for i = 0, record_get_count(2, 60) - 1 do
                    local cur_read_record = record_read(2, 60, i)
                    if string.find(cur_read_record, delete_device, 1) ~= nil then
                        record_delete(2, 60, i)
                        break
                    end
                end
            end
            -- 移除继电器
            print("施肥组剩余设备:" .. record_get_count(2, 60))
            record_delete(3, 11, delete_record_value)
            -- 移除待工作继电器
            if record_get_count(0, 60) ~= 0 then
                for i = 0, record_get_count(0, 60) - 1 do
                    local cur_read_record = record_read(0, 60, i)
                    if string.find(cur_read_record, delete_device, 1) ~= nil then
                        record_delete(0, 60, i)
                        i = 0
                    end
                end
            end
        end
        if delete_group == "打药组" then
            if record_get_count(2, 4) > 0 then
                for i = 0, record_get_count(2, 4) - 1 do
                    local cur_read_record = record_read(2, 4, i)
                    if string.find(cur_read_record, delete_device, 1) ~= nil then
                        record_delete(2, 4, i)
                        break
                    end
                end
            end
            -- 移除继电器
            print("打药组剩余设备:" .. record_get_count(2, 4))
            record_delete(3, 11, delete_record_value)
            -- 移除待工作继电器
            if record_get_count(0, 19) ~= 0 then
                for i = 0, record_get_count(0, 19) - 1 do
                    local cur_read_record = record_read(0, 19, i)
                    if string.find(cur_read_record, delete_device, 1) ~= nil then
                        record_delete(0, 19, i)
                        i = 0
                    end
                end
            end
        end
        if delete_group == "水泵组" then
            if record_get_count(2, 7) > 0 then
                for i = 0, record_get_count(2, 7) - 1 do
                    local cur_read_record = record_read(2, 7, i)
                    if string.find(cur_read_record, delete_device, 1) ~= nil then
                        record_delete(2, 7, i)
                        break
                    end
                end
            end
            -- 移除
            print("水泵组剩余设备:" .. record_get_count(2, 7))
            record_delete(3, 11, delete_record_value)
            -- 移除待工作继电器
            if record_get_count(0, 23) ~= 0 then
                for i = 0, record_get_count(0, 23) - 1 do
                    local cur_read_record = record_read(0, 23, i)
                    if string.find(cur_read_record, delete_device, 1) ~= nil then
                        record_delete(0, 23, i)
                        i = 0
                    end
                end
            end
            if record_get_count(0, 36) ~= 0 then
                for i = 0, record_get_count(0, 36) - 1 do
                    local cur_read_record = record_read(0, 36, i)
                    if string.find(cur_read_record, delete_device, 1) ~= nil then
                        record_delete(0, 36, i)
                        i = 0
                    end
                end
            end
        end
    end

    -- 画面3：【打开】继电器
    if screen == 3 and control == 21 and value == 1 then
        -- 读取组别
        local group = get_text(3, 42)
        -- 读取选中设备名
        local name = record_read(3, 11, delete_record_value)
        open_device(group, name)
    end

    -- 画面3：【关闭】继电器
    if screen == 3 and control == 17 and value == 1 then
        -- 读取组别
        local group = get_text(3, 42)
        -- 读取选中设备名
        local name = record_read(3, 11, delete_record_value)
        close_device(group, name)
    end

end

-- 入队列
function in_queue(queue, in_data)
    local queue_num = queue["num"]
    queue[queue_num] = in_data
    queue["num"] = queue["num"] + 1
end

-- 出队列
function out_queue(queue)
    local queue_num = queue["num"]
    if queue_num ~= 0 then
        local return_data = queue[0]
        if queue_num >= 2 then
            for i = 0, queue_num - 2 do
                queue[i] = queue[i + 1]
            end
        end
        queue[queue_num - 1] = 0
        queue["num"] = queue["num"] - 1
        return return_data
    end
end

-- 查询设备在线及状态
function query_device_isonline(ip)
    if net_status == 1 then
        if type(ip) == type("string") then
            http_request(1, "http://192.168.0." .. ip .. "/online", 0)
        end
        if type(ip) == type(1) then
            http_request(1, "http://192.168.0." .. tostring(ip) .. "/online", 0)
        end
    end
end

-- 打开继电器设备 EE B5 02 00 01(ip) 00(id) FC FF
function open_device(group, name)
    in_queue(open_group_queue, group)
    in_queue(open_name_queue, name)
end
function open_mydevice()
    local ip
    local id

    if open_group == "施肥组" then
        for i = 0, record_get_count(2, 60) - 1 do
            local cur_read_record = record_read(2, 60, i)
            if string.find(cur_read_record, open_name .. ";", 1) ~= nil then
                ip = string.sub(cur_read_record, 1, 3)
                id = string.sub(string.match(cur_read_record, ";%d;"), 2, 2)
                break
            end
        end
    end
    if open_group == "打药组" then
        for i = 0, record_get_count(2, 4) - 1 do
            local cur_read_record = record_read(2, 4, i)
            if string.find(cur_read_record, open_name .. ";", 1) ~= nil then
                ip = string.sub(cur_read_record, 1, 3)
                id = string.sub(string.match(cur_read_record, ";%d;"), 2, 2)
                break
            end
        end
    end
    if open_group == "水泵组" then
        for i = 0, record_get_count(2, 7) - 1 do
            local cur_read_record = record_read(2, 7, i)
            if string.find(cur_read_record, open_name .. ";", 1) ~= nil then
                ip = string.sub(cur_read_record, 1, 3)
                id = string.sub(string.match(cur_read_record, ";%d;"), 2, 2)
                break
            end
        end
    end

    -- 发送请求
    if net_status == 1 then
        http_request(1, "http://192.168.0." .. ip .. "/on" .. id, 0)
    end

    -- 日志记录
    record(open_name, "打开")
end
-- 关闭继电器设备 EE B5 03 00 01(ip) 00(id) FC FF
function close_device(group, name)
    in_queue(close_group_queue, group)
    in_queue(close_name_queue, name)
end
function close_mydevice()
    local ip
    local id

    if close_group == "施肥组" then
        for i = 0, record_get_count(2, 60) - 1 do
            local cur_read_record = record_read(2, 60, i)
            if string.find(cur_read_record, close_name .. ";", 1) ~= nil then
                ip = string.sub(cur_read_record, 1, 3)
                ip = tonumber(ip)
                id = string.sub(string.match(cur_read_record, ";%d;"), 2, 2)
                break
            end
        end
    end
    if close_group == "打药组" then
        for i = 0, record_get_count(2, 4) - 1 do
            local cur_read_record = record_read(2, 4, i)
            if string.find(cur_read_record, close_name .. ";", 1) ~= nil then
                ip = string.sub(cur_read_record, 1, 3)
                ip = tonumber(ip)
                id = string.sub(string.match(cur_read_record, ";%d;"), 2, 2)
                break
            end
        end
    end
    if close_group == "水泵组" then
        for i = 0, record_get_count(2, 7) - 1 do
            local cur_read_record = record_read(2, 7, i)
            if string.find(cur_read_record, close_name .. ";", 1) ~= nil then
                ip = string.sub(cur_read_record, 1, 3)
                ip = tonumber(ip)
                id = string.sub(string.match(cur_read_record, ";%d;"), 2, 2)
                break
            end
        end
    end

    -- 发送请求
    if net_status == 1 then
        http_request(1, "http://192.168.0." .. ip .. "/off" .. id, 0)
    end

    -- 日志记录
    record(close_name, "关闭")
end

-- 定时器回调函数
function on_timer(timer_id)

    -- 定时器0：施肥定时器
    if timer_id == 0 then
        shifei_timer_count_sec = shifei_timer_count_sec+1;
        if shifei_timer_count_sec == 60 then
            shifei_timer_count_sec = 0
            shifei_timer_count = shifei_timer_count + 1;
        end
        if shifei_timer_count == shifei_time and shifei_timer_count_sec == shifei_time_sec then
            shifei_timer_count = 0;
            shifei_timer_count_sec = 0;
            -- 查找工作中设备，修改状态：已结束
            local record
            local working_device
            for i = 0, record_get_count(0, 60) - 1 do
                record = record_read(0, 60, i)
                if string.find(record, "工作中", 1) ~= nil then
                    local new_record = string.gsub(record, "工作中", "已结束", 1);
                    local name = string.match(record, "施肥%d+")
                    record_modify(0, 60, i, new_record);
                    close_device("施肥组", name)
                    working_device = i
                    break
                end
            end
            -- 如果是最后一个设备工作结束
            if working_device == record_get_count(0, 60) - 1 then
                stop_timer(0);
                shifei_isWorking = 0
                set_enable(0, 1, 1)
                print("施肥结束");
            else
                -- 下一个设备启动,修改未开始：工作中
                local new_record = record_read(0, 60, working_device + 1);
                new_record = string.gsub(new_record, "未开始", "工作中", 1);
                record_modify(0, 60, working_device + 1, new_record)
                local name = string.match(new_record, "施肥%d+")
                print("工作中:" .. name)
                open_device("施肥组", name)
            end
        end
    end
    -- 定时器1：打药定时器
    if timer_id == 1 then
        dayao_timer_count_sec = dayao_timer_count_sec + 1;
        if dayao_timer_count_sec == 60 then
            dayao_timer_count_sec=0;
            dayao_timer_count = dayao_timer_count + 1;
        end
        if dayao_timer_count == dayao_time and dayao_timer_count_sec == dayao_time_sec then
            dayao_timer_count_sec=0;
            dayao_timer_count = 0;
            -- 查找工作中设备，修改状态：已结束
            local record
            local working_device
            for i = 0, record_get_count(0, 19) - 1 do
                record = record_read(0, 19, i)
                if string.find(record, "工作中", 1) ~= nil then
                    local new_record = string.gsub(record, "工作中", "已结束", 1);
                    local name = string.match(record, "打药%d+")
                    record_modify(0, 19, i, new_record);
                    close_device("打药组", name)
                    working_device = i
                    break
                end
            end

            -- 如果是最后一个设备工作结束
            if working_device == record_get_count(0, 19) - 1 then
                stop_timer(1);
                dayao_isWorking = 0
                set_enable(0, 3, 1)
                print("打药结束");
            else
                -- 下一个设备启动,修改未开始：工作中
                local new_record = record_read(0, 19, working_device + 1);
                new_record = string.gsub(new_record, "未开始", "工作中", 1);
                record_modify(0, 19, working_device + 1, new_record)
                local name = string.match(new_record, "打药%d+")
                print("工作中:" .. name)
                open_device("打药组", name)
            end
        end
    end
    -- 定时器2：水泵施肥定时器
    if timer_id == 2 then
        shuibeng_shifei_timer_count_sec = shuibeng_shifei_timer_count_sec + 1;
        if shuibeng_shifei_timer_count_sec == 60 then
            shuibeng_shifei_timer_count_sec=0;
            shuibeng_shifei_timer_count=shuibeng_shifei_timer_count+1;
        end
        if shuibeng_shifei_timer_count == shuibeng_shifei_time and shuibeng_shifei_timer_count_sec == shuibeng_shifei_time_sec then
            shuibeng_shifei_timer_count = 0;
            shuibeng_shifei_timer_count_sec = 0;
            shuibeng_shifei_isWorking = 0
            stop_timer(2);
            set_enable(0, 24, 1)
            -- 关闭全部继电器，修改正在工作状态：关
            for i = 0, record_get_count(0, 23) - 1 do
                local cur_read_record = record_read(0,23,i)
                local name = string.match(cur_read_record, "水泵施肥%d+")
                local new_record = name .. ";关;"
                record_modify(0, 23, i, new_record)
                close_device("水泵组", name)
            end
            print("水泵施肥结束");
        end
    end
    -- 定时器3：水泵打药定时器
    if timer_id == 3 then
        shuibeng_dayao_timer_count_sec = shuibeng_dayao_timer_count_sec + 1;
        if shuibeng_dayao_timer_count_sec == 60 then
            shuibeng_dayao_timer_count_sec = 0;
            shuibeng_dayao_timer_count = shuibeng_dayao_timer_count + 1;
        end
        if shuibeng_dayao_timer_count == shuibeng_dayao_time and shuibeng_dayao_timer_count_sec == shuibeng_dayao_time_sec then
            shuibeng_dayao_timer_count = 0;
            shuibeng_dayao_timer_count_sec=0;
            shuibeng_dayao_isWorking = 0
            stop_timer(3);
            set_enable(0, 33, 1)
            -- 关闭全部继电器，修改正在工作状态：关
            for i = 0, record_get_count(0, 36) - 1 do
                local cur_read_record = record_read(0,36,i)
                local name = string.match(cur_read_record, "水泵打药%d+")
                local new_record = name .. ";关;"
                record_modify(0, 36, i, new_record)
                close_device("水泵组", name)
            end
            print("水泵打药结束");
        end
    end
	
	if timer_id == 4 then
		
        -- 施肥信息
        local shifei = {
            ["min"]=shifei_time,
            ["sec"]=shifei_time_sec
        }
 		local dayao = {
            ["min"]=dayao_time,
            ["sec"]=dayao_time_sec
        }
		local shuibeng_shifei = {
            ["min"]=shuibeng_shifei_time,
            ["sec"]=shuibeng_shifei_time_sec
        }
 		local shuibeng_dayao = {
            ["min"]=shuibeng_dayao_time,
            ["sec"]=shuibeng_dayao_time_sec
        }	
        -- table.insert(shifei, shifei_time)
        -- 施肥
        local dQueue1 = {}
        -- 打药
        local dQueue2 = {}
        -- 水泵
        local dQueue3 = {}
		local log = {}
        for i = 0, record_get_count(2, 60) - 1 do
            local cur_read_record = record_read(2,60,i)
 			table.insert(dQueue1, cur_read_record)	
        end
        for i = 0, record_get_count(2, 4) - 1 do
            local cur_read_record = record_read(2,4,i)
 			table.insert(dQueue2, cur_read_record)	
        end
        for i = 0, record_get_count(2, 7) - 1 do
            local cur_read_record = record_read(2,7,i)
 			table.insert(dQueue3, cur_read_record)	
        end
 		for i = 0, record_get_count(6, 1) - 1 do
            local cur_read_record = record_read(6,1,i)
 			table.insert(log, cur_read_record)	
        end	
		local property ={};
        -- property["shifei"] = shifei

	 	local lua_object1 = {
			["version"] = version,
		    ["shifeiTime"] = shifei,
 			["dayaoTime"] = dayao,
			["shuibengShifeiTime"] = shuibeng_shifei,
 			["shuibengDayaoTime"] = shuibeng_dayao,
 			["shifeiState"] = dQueue1,
 			["dayaoState"] = dQueue2,
			["shuibengState"] = dQueue3,		
			["log"] = log
		}
		local dataStr = cjson.encode(lua_object1)
		local lua_object3 = {
			["deviceId"] = deviceId,
		    ["data"] = dataStr
		}	
        local jsonStr = cjson.encode(lua_object3)	
		-- local jsonStr = cjson.encode(property)	
		print(jsonStr);
 		if net_status == 2 then	
			http_request(7,"http://pm1.eshare.center/uploadScreenData",1,"application/json;charset=GBK",jsonStr)
		end
	end
end

-- 检查设备在线及状态
function check_status()
    -- 检查该IP是否已经初始化了
    local init_done_ip = 0
    if record_get_count(2, 60) ~= 0 then
        for i = 0, record_get_count(2, 60) - 1 do
            local cur_read_record = record_read(2, 60, i)
            local cur_ip = string.match(cur_read_record, "%d+")
            if cur_ip ~= init_done_ip then
                in_queue(query_queue, cur_ip)
                init_done_ip = cur_ip
            end
            if string.find(cur_read_record, '在', 1) ~= nil then
                local new_record = string.gsub(cur_read_record, '在', '离', 1)
                record_modify(2, 60, i, new_record)
            end
        end
    end
    init_done_ip = 0
    if record_get_count(2, 4) ~= 0 then
        for i = 0, record_get_count(2, 4) - 1 do
            local cur_read_record = record_read(2, 4, i)
            local cur_ip = string.match(cur_read_record, "%d+")
            if cur_ip ~= init_done_ip then
                in_queue(query_queue, cur_ip)
                init_done_ip = cur_ip
            end
            if string.find(cur_read_record, '在', 1) ~= nil then
                local new_record = string.gsub(cur_read_record, '在', '离', 1)
                record_modify(2, 4, i, new_record)
            end
        end
    end
    init_done_ip = 0
    if record_get_count(2, 7) ~= 0 then
        for i = 0, record_get_count(2, 7) - 1 do
            local cur_read_record = record_read(2, 7, i)
            local cur_ip = string.match(cur_read_record, "%d+")
            if cur_ip ~= init_done_ip then
                in_queue(query_queue, cur_ip)
                init_done_ip = cur_ip
            end
            if string.find(cur_read_record, '在', 1) ~= nil then
                local new_record = string.gsub(cur_read_record, '在', '离', 1)
                record_modify(2, 7, i, new_record)
            end
        end
    end
end

function record(name, opra)
    local year, mon, day, hour, min, sec, week = get_date_time()
    local new_record = mon .. '/' .. day .. ' ' .. hour .. ':' .. min .. ':' .. sec .. ';' .. name .. ';' .. opra .. ';'
    record_add(6, 1, new_record)
end