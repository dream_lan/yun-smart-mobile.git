import Vue from 'vue'
import App from './App.vue'
import Vant from 'vant';
import 'vant/lib/index.css';
import router from './router'
import VueResource from 'vue-resource'
import vcolorpicker from 'vcolorpicker'
import md5 from 'js-md5';
import * as echarts from 'echarts';
import mqtt from './api/mqtt'
import mqttcontrol from './api/mqttcontrol'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import AMap from 'vue-amap';
import VideoPlayer from 'vue-video-player'
import 'video.js/dist/video-js.css'
import 'vue-video-player/src/custom-theme.css'
import store from './store'
import { Dialog } from "vant"
Vue.use(Dialog)
Vue.use(VideoPlayer)
// Vue.use(Popover)
Vue.prototype.resetSetItem = function (key, newVal) {
  if (key === 'Background') {

      // 创建一个StorageEvent事件
      var newStorageEvent = document.createEvent('StorageEvent');
      const storage = {
          setItem: function (k, val) {
              localStorage.setItem(k, val);

              // 初始化创建的事件
              newStorageEvent.initStorageEvent('setItem', false, false, k, null, val, null, null);

              // 派发对象
              window.dispatchEvent(newStorageEvent)
          }
      }
      return storage.setItem(key, newVal);
  }
}
Vue.use(AMap);

// 初始化vue-amap
AMap.initAMapApiLoader({
  // 高德key
  key: '8c4b2622f0420d26c2d052448c8748c6',
  // 插件集合 （插件按需引入）
  plugin: ['AMap.Geolocation','AMap.Scale', 'AMap.OverView', 'AMap.ToolBar', 'AMap.MapType'],
  uiVersion: '1.0', // ui库版本，不配置不加载,
  v: '1.4.4'
});


Vue.prototype.$version = '1.9.7'
Vue.prototype.$mqtt = mqtt
Vue.prototype.$mqttcontrol = mqttcontrol
Vue.prototype.$md5 = md5;
Vue.use(vcolorpicker)
Vue.use(echarts)
Vue.use(VueResource)
Vue.use(ElementUI)
//import store from './store'
Vue.use(Vant);
Vue.config.productionTip = false
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')