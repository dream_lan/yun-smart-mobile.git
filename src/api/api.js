import {
    getAction,
    deleteAction,
    putAction,
    postAction,
    downFileAction,
    fileUploadAction
} from '@/api/manage'

const getOtaInfo = (params) => getAction("/queryOtaInfo", params);
const sendVeryCode = (params) => getAction("/sendVeryCode", params);
const addUserByEmail = (params) => getAction("/addUserByEmail", params);
const updatePwdByEmail = (params) => getAction("/updatePwdByEmail", params);
const deleteFunction = (params) => getAction("/deleteDeviceInterfaceInfo", params);
const updateFunction = (params) => getAction("/updateDeviceInterfaceInfo", params);
const addFunction = (params) => getAction("/addDeviceInterface", params);
const queryStatisticsInfo = (params) => getAction("/queryStatisticsInfoByID", params);
const queryUserByuserId = (params) => getAction("/queryUserByuserId", params);
const queryUserDetailById = (params) => getAction("/queryUserDetailById", params);
const updateUserDetail = (params) => postAction("/updateUserDetail", params);
const editDevice = (params) => getAction("/editDevice", params);


const queryDeviceCommentInfo = (params) => getAction("/queryDeviceCommentInfo", params);
const deleteDeviceComment = (params) => getAction("/deleteDeviceComment", params);
const addDeviceComment = (params) => getAction("/addDeviceComment", params);


const getTest = (params) => getAction("/api/user/get", params);
const deleteActionTest = (params) => deleteAction("/api/user/delete", params);
const putActionTest = (params) => putAction("/api/user/put", params);
const postActionTest = (params) => postAction("/api/user/post", params);
const downFileActionTest = (params) => downFileAction("/api/user/downfile", params);
const fileUploadActionTest = (params) => fileUploadAction("/api/user/fileupload", params);


export {
    getOtaInfo,
    sendVeryCode,
    addUserByEmail,
    updatePwdByEmail,
    addFunction,
    updateFunction,
    deleteFunction,
    queryStatisticsInfo,
    queryUserByuserId,
    queryDeviceCommentInfo,
    deleteDeviceComment,
    addDeviceComment,
    queryUserDetailById,
    updateUserDetail,
    editDevice,

    getTest,
    deleteActionTest,
    putActionTest,
    postActionTest,
    downFileActionTest,
    fileUploadActionTest
}